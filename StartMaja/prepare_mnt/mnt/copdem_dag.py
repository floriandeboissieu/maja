# -*- coding: utf-8 -*-
# Copyright (C) 2022 CS GROUP France
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" Data Access Gateway for Copernicus DEM tiles available on AWS

Author: Mickael SAVINAUD - mickael dot savinaud at csgroup dot eu
"""
import logging
from pathlib import Path
from tempfile import gettempdir
from typing import List

import boto3
from botocore import UNSIGNED
from botocore.exceptions import ClientError
from botocore.client import Config
from eotile.eotile_module import main


logger = logging.getLogger(__name__)

_COPDEM_RESOLUTIONS = ["1s", "3s"]


class AWSCopDEMBucket:
    """Class to handle access to Copdem data
    from AWS open data bucket: https://registry.opendata.aws/copernicus-dem/"""

    def __init__(self, resolution="1s") -> None:
        self._s3_client = boto3.client("s3", config=Config(signature_version=UNSIGNED))
        if resolution == "1s":
            self._bucket_name = "copernicus-dem-30m"
            self._copdem_prefix = "Copernicus_DSM_COG_10_"
        elif resolution == "3s":
            self._bucket_name = "copernicus-dem-90m"
            self._copdem_prefix = "Copernicus_DSM_COG_30_"
        else:
            ValueError("Resolution of Copernicus DEM is 1s or 3s!")

        self._copdem_suffix = "_00_DEM"

    def download_tiles(
        self,
        copdem_tile_ids: List[str],
        out_dirpath: Path = Path(gettempdir()),
        to_sen2cor: bool = False,
    ) -> None:
        """
        Download copdem tiles from AWS bucket
        :param copdem_tile_ids: List of copdem tile ids
        :param out_dirpath: Output directory
        :param to_sen2cor: If True, rename copdem files to match sen2cor expectations
        :return: None
        """

        for copdem_tile_id in copdem_tile_ids:
            copdem_tile_id_aws = (
                self._copdem_prefix
                + copdem_tile_id[:3]
                + "_00_"
                + copdem_tile_id[3:]
                + self._copdem_suffix
            )
            copdem_tile_id_filename = copdem_tile_id_aws + ".tif"
            copdem_tile_id_filepath = (
                out_dirpath + "/" + copdem_tile_id_filename.replace("_COG_", "_")
            )
            copdem_object_key = copdem_tile_id_aws + "/" + copdem_tile_id_filename
            logger.info(
                "Try to download %s to %s", copdem_object_key, copdem_tile_id_filepath
            )
            try:
                self._s3_client.download_file(
                    Bucket=self._bucket_name,
                    Key=copdem_object_key,
                    Filename=str(copdem_tile_id_filepath),
                )
            except ClientError as e:
                logger.error(e)
                pass

    def _compute_key(self, copdem_tile_id: str) -> str:
        copdem_tile_id_aws = f"{self._copdem_prefix}{copdem_tile_id[:3]}\
_00_{copdem_tile_id[3:]}{self._copdem_suffix}"
        copdem_tile_id_filename = f"{copdem_tile_id_aws}.tif"
        return f"{copdem_tile_id_aws}/{copdem_tile_id_filename}"

    def to_gdal_path(self, copdem_tile_id: str) -> str:
        return f"/vsis3/{self._bucket_name}/{self._compute_key(copdem_tile_id)}"


def get_copdem_from_s2_tile_id(
    s2_tile_id: str,
    out_dirpath: Path = Path(gettempdir()),
    resolution: str = _COPDEM_RESOLUTIONS[0],
) -> None:
    """
    Retrieve copdem data for a Sentinel-2 tile id from aws into the output dir
    :param s2_tile_ids: Sentinel-2 tile id
    :param source: Source where to retrieve the copdem 1s data
    :param resolution: 1s or 3s for respectively 30m and 90m copdem
    """
    get_copdem_tiles(
        get_copdem_ids(s2_tile_id),
        out_dirpath,
        resolution=resolution,
    )


def get_copdem_tiles(
    copdem_tile_ids: List[str],
    out_dir: Path = Path(gettempdir()),
    resolution: str = _COPDEM_RESOLUTIONS[0],
) -> None:
    """
    Retrieve copdem data from the source into the output dir
    :param copdem_tile_ids: List of copdem tile ids
    :param out_dirpath: Output directory where the copdem data is downloaded
    :param resolution: 1s or 3s for respectively 30m and 90m copdem
    """
    AWSCopDEMBucket(resolution=resolution).download_tiles(copdem_tile_ids, out_dir)


def get_copdem_ids(s2_tile_id: str) -> List[str]:
    """
    Get copdem id for an S2 tile
    :param s2 tile_id:
    :return: List of copdem ids
    """
    res = main(s2_tile_id, dem=True, overlap=True, no_l8=True, no_s2=True)
    return list(res[2].id)


def get_gdal_vrt_files(
    copdem_tile_ids: List[str],
    resolution: str = _COPDEM_RESOLUTIONS[0],
) -> List[str]:
    """Compute vsis3 files for the COP DEM files from the AWS bucket

    Args:
        copdem_tile_ids (List[str]): List of COP DEM tile ID
        resolution (str, optional): Resolution of the COP DEM. Defaults to _COPDEM_RESOLUTIONS[0].

    Returns:
        List[str]: List of vsis3 file for gdal commands
    """
    copdem_gdal_paths = []
    copdem_bucket = AWSCopDEMBucket(resolution=resolution)
    for copdem_tile_id in copdem_tile_ids:
        copdem_gdal_paths.append(copdem_bucket.to_gdal_path(copdem_tile_id))

    return copdem_gdal_paths


def to_gdal_vrt_input_file_list(
    copdem_tile_ids: List[str],
    resolution: str = _COPDEM_RESOLUTIONS[0],
    filepath: Path = Path(gettempdir()) / "copdem_list.txt",
) -> None:
    """Write gdalbuildvrt file with vsis3 file according to the COP DEM tile id

    Args:
        copdem_tile_ids (List[str]): List of COP DEM tile ID
        resolution (str, optional):  Resolution of the COP DEM. Defaults to _COPDEM_RESOLUTIONS[0].
        filepath (Path, optional): Filepath where to write the vsis3 file.
            Defaults to Path(gettempdir())/"copdem_list.txt".
    """
    with open(filepath, "wt", encoding="UTF8") as out_txt_file:
        out_txt_file.write(
            "\n".join(get_gdal_vrt_files(copdem_tile_ids, resolution=resolution))
        )


if __name__ == "__main__":
    import sys

    LOG_FORMAT = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stdout,
        format=LOG_FORMAT,
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    _TILE_ID = "31TCJ"
    logger.info(get_copdem_ids(_TILE_ID))
    logger.info(get_gdal_vrt_files(get_copdem_ids(_TILE_ID)))
    to_gdal_vrt_input_file_list(get_copdem_ids(_TILE_ID))
    get_copdem_from_s2_tile_id(_TILE_ID)
    get_copdem_from_s2_tile_id(_TILE_ID, resolution=_COPDEM_RESOLUTIONS[1])

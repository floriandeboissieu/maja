#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from StartMaja.prepare_mnt.mnt.MNTBase import MNT
import logging
import math
from StartMaja.Common import ImageTools
import StartMaja.prepare_mnt.mnt.copdem_dag as AWSCopDEM
from pathlib import Path

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)



class CopDEMGLO30(MNT):
    """
    Base class to get an  DEM/MNT for a given site.
    """

    def __init__(self, site, **kwargs):
        """
        Initialise an CopDEMGLO30-type DEM.

        :param site: The :class:`prepare_mnt.mnt.SiteInfo` struct containing the basic information.
        :param kwargs: Forwarded parameters to :class:`prepare_mnt.mnt.MNTBase`
        """
        super(CopDEMGLO30, self).__init__(site, **kwargs)
        self.CopDEMGLO30_codes, self.aws_ids = self.get_CopDEMGLO30_codes(self.site)
        if not self.dem_version:
            self.dem_version = 1001

    def get_raw_data(self):
        """
        Get the DEM raw-data from a given directory. If not existing, an attempt will be made to download
        it automatically.

        :return: A list of filenames containing the raw DEM data.
        :rtype: list of str
        """

        filenames = []
        ids_to_download = []
        aws_filenames = []
        for code, id in zip(self.CopDEMGLO30_codes, self.aws_ids):
            output_path_dt2 = Path(self.raw_dem, code)
            output_path_tif = Path(self.raw_dem, code.replace("dt2", "tif"))
            if not (output_path_dt2.is_file()):
                # if the dem is not found on the raw_dem path, try to download it from aws
                if not (output_path_tif.is_file()):
                    logger.info("Dowwloading {}".format(output_path_tif))
                    ids_to_download.append(id)
                filenames.append(str(output_path_tif))
            else:
                filenames.append(str(output_path_dt2))
        if len(ids_to_download) > 0:
            # Trying to download the missing dem files from s3 for the tile
            AWSCopDEM.get_copdem_tiles(
                ids_to_download, out_dir=self.raw_dem, resolution="1s"
            )
        # Check if filenames are actually existing and remove if not
        filenames = [f for f in filenames if Path(f).is_file()]
        return filenames

    def prepare_mnt(self):
        """
        Prepare the CopDEMGLO30 files.

        :return: Path to the full resolution DEM file.gsw
        :rtype: str
        """
        # Find/Download CopDEMGLO30 files:
        CopDEMGLO30_files = self.get_raw_data()
        # Combine to image of fixed extent
        CopDEMGLO30_full_res = str(
            Path(self.wdir, "CopDEMGLO30_%sm.tif" % int(self.site.res_x))
        )
        ImageTools.gdal_warp(
            src=CopDEMGLO30_files,
            dst=CopDEMGLO30_full_res,
            r="cubic",
            te=self.site.te_str,
            t_srs=self.site.epsg_str,
            tr=self.site.tr_str,
            dstnodata=0,
            srcnodata=-32768,
            multi=True,
        )
        return CopDEMGLO30_full_res

    @staticmethod
    def get_CopDEMGLO30_codes(site):
        """
        Get the list of CopDEMGLO30 files for a given site.
        :return: The list of filenames needed in order to cover to whole site.
        """
        latmin = int(math.floor(site.lr_lonlat[1]))
        latmax = int(math.ceil(site.ul_lonlat[1]))
        lonmin = int(math.floor(site.ul_lonlat[0] % 360.0))
        lonmax = int(math.ceil(site.lr_lonlat[0] % 360.0))
        if lonmax < lonmin:
            lonmax += 360

        CopDEMGLO30_files = []
        CopDEMGLO30_aws_ids = []
        for lat_loc in range(latmin, latmax):
            lat_tag = CopDEMGLO30.get_latitude_tag(lat_loc)
            for lon_loc in range(lonmin, lonmax):
                lon_tag = CopDEMGLO30.get_longitude_tag(lon_loc)
                CopDEMGLO30_files.append(
                    "Copernicus_DSM_10_%s_00_%s_00_DEM.dt2" % (lat_tag, lon_tag)
                )
                CopDEMGLO30_aws_ids.append(lat_tag + lon_tag)

        return CopDEMGLO30_files, CopDEMGLO30_aws_ids

    @staticmethod
    def get_latitude_tag(value):
        assert isinstance(value, int)
        assert -90 <= value <= 90
        if value < 0:
            return "S%02d" % (-value)
        else:
            return "N%02d" % (value)

    @staticmethod
    def get_longitude_tag(value):
        assert isinstance(value, int)
        val = value % 360
        if val > 180:
            val -= 360
        if val < 0:
            return "W%03d" % (-val)
        else:
            return "E%03d" % (val)


if __name__ == "__main__":
    pass

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2016-2020 Centre National d'Etudes Spatiales (CNES), CSSI, CESBIO  All Rights Reserved

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os, shutil, tempfile
from osgeo import gdal


def compress_geotiff_file(
    src_file, dest_file=None, tiled=True, compress="deflate", zlevel=4, predictor=1
):
    """
    Compress a geotiff file using internal tif compression. It allows for fewer space to be used on disk and almost always also speeds up read because the disk is usually the limiting factor.
    :param src_file: str path to source file
    :param dest_file: (optional) destination path. If not filled, file will be compressed "inplace" i.e. first to a temp location than moved to replace src_file
    :param tiled: (optional) boolean to activate tiling
    :param compress: (optional) compression algorithm : deflate (default), lzw or lzma
    :param zlevel: (optional) deflate compression level, default is 4
    :param predictor: (optional) predictor level, default is 1 (really not that useful to my humble experience)
    """
    if dest_file is None:
        dest_file = src_file
    compression_options = []
    if tiled:
        compression_options.append("tiled=yes")
    compression_options.append("compress=%s" % compress)
    if compress == "deflate":
        compression_options.append("zlevel=%d" % zlevel)
    if compress in ["deflate", "lzw", "lzma"]:
        compression_options.append("predictor=%d" % predictor)
    if os.path.abspath(src_file) == os.path.abspath(dest_file):
        prefix, sufix = ".".join(dest_file.split(".")[0:-1]), dest_file.split(".")[-1]
        temp_name = prefix + "_temp." + sufix
        gdal.Translate(temp_name, src_file, creationOptions=compression_options)
        shutil.move(temp_name, dest_file)
    else:
        gdal.Translate(dest_file, src_file, creationOptions=compression_options)


def replicate_directory_with_compressed_tiffs(
    input_fol,
    output_fol=None,
    tiled=True,
    compress="deflate",
    zlevel=4,
    predictor=1,
    verbose=0,
):
    """
    Replicates a directory and activates internal compression for all tif files resulting in an overall lighter directory.
    :param input_fol: str path to input folder
    :param output_fol: (optional) destination folder path. If not filled, folder will be compressed "inplace" i.e. first to a temp location than moved to replace input folder
    :param tiled: (optional) boolean to activate tiling
    :param compress: (optional) compression algorithm : deflate (default), lzw or lzma
    :param zlevel: (optional) deflate compression level, default is 4
    :param predictor: (optional) predictor level, default is 1 (really not that useful to my humble experience)
    :param verbose: (optional) verbose level, default is 0 i.e. no information is printed
    """

    in_place_mode = output_fol is None

    # if input is a file, only convert this single file
    if os.path.isfile(input_fol):
        if in_place_mode:
            output_fol = input_fol
        compress_geotiff_file(
            input_fol,
            dest_file=output_fol,
            tiled=tiled,
            compress=compress,
            zlevel=zlevel,
            predictor=predictor,
        )
        return

    # parse directory and convert all geotiff files
    if in_place_mode:
        output_fol = tempfile.mkdtemp(
            prefix=os.path.basename(input_fol) + "_temp_",
            dir=os.path.dirname(input_fol),
        )
    if os.path.exists(output_fol):
        shutil.rmtree(output_fol)
    os.makedirs(output_fol)
    for root_src, dirs, files in os.walk(input_fol, topdown=True):
        root_target = root_src.replace(input_fol, output_fol)
        for name in files:
            src_file = os.path.join(root_src, name)
            target_file = os.path.join(root_target, name)
            if os.path.exists(target_file):
                continue
            if verbose > 0:
                print("%s -> %s" % (src_file, target_file))
            if src_file.lower().split(".")[-1] in ["tiff", "tif"]:
                compress_geotiff_file(
                    src_file,
                    dest_file=target_file,
                    tiled=tiled,
                    compress=compress,
                    zlevel=zlevel,
                    predictor=predictor,
                )
            else:
                shutil.copy(src_file, target_file)
        for name in dirs:
            target_dir = os.path.join(root_target, name)
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)
    if in_place_mode:
        shutil.rmtree(input_fol)
        shutil.move(output_fol, input_fol)

// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-144719-CS : 17 mai 2016 : Correction warning et contrôle qualité      *
 * VERSION : 4-4-1 : FA : LAIG-FA-MAC-127944-CS : 9 juillet 2015 : Correction warning et contrôle qualité   *
 * 														sur le code source C++   							*
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 13 mars 2014 : Modifications mineures                     *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 30 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL1QuicklookTool_txx
#define __vnsL1QuicklookTool_txx

#include "vnsL1QuicklookTool.h"
//#include "vnsCaching.h"

namespace vns
{

    /** Constructor */
    template<class TInputImage, class TOutputImage>
        L1QuicklookTool<TInputImage, TOutputImage>::L1QuicklookTool() :
                m_ULIndexX(0), m_ULIndexY(0), m_NbLinesX(0), m_NbLinesY(0), m_Extract(false), m_QLRedBand(3), m_QLGreenBand(2), m_QLBlueBand(
                        1)
        {
            /** default value */
            m_Ratio = 1;
            m_NoData = -10000;
            m_NoDataReplaceValue = 0;
            m_OutputMin = 0;
            m_OutputMax = 255;

            // Init reflectance
            m_MinReflBRed = static_cast<InputImageInternalPixelType>(0);
            m_MaxReflBRed = static_cast<InputImageInternalPixelType>(0);
            m_MinReflBGreen = static_cast<InputImageInternalPixelType>(0);
            m_MaxReflBGreen = static_cast<InputImageInternalPixelType>(0);
            m_MinReflBBlue = static_cast<InputImageInternalPixelType>(0);
            m_MaxReflBBlue = static_cast<InputImageInternalPixelType>(0);

            /** Create filters */
            m_MultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_ChangeValueVectorImageFilter = ChangeValueVectorImageFilterType::New();
            m_QuicklookGenerator = QuicklookImageGeneratorType::New();
            m_VectorRescaleIntensityImageFilter = VectorRescaleIntensityImageFilterType::New();
            m_WriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();
            m_VectorRescaleIntensityImageFilterReader = OutputImageFileReaderType::New();

            m_Method = QUICKLOOK;

            m_OutputImage = OutputImageType::New();

        }

    /** Destructor */
    template<class TInputImage, class TOutputImage>
        L1QuicklookTool<TInputImage, TOutputImage>::~L1QuicklookTool()
        {
        }

    template<class TInputImage, class TOutputImage>
        void
        L1QuicklookTool<TInputImage, TOutputImage>::CheckInputs(void)
        {
            // Grab the input
            InputImageConstPointer inputImage = this->GetTOAInputImage();
            // Checks if valid
            if (inputImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input image.");
            }

            // If the method is QUICKLOOK, checks the ratio value
            if ((m_Method == QUICKLOOK) && (m_Ratio == 0))
            {
                vnsExceptionBusinessMacro("Impossible to compute a quicklook with a null ratio. The ratio has to be different of 0");
            }
            else
            {
                // If the method is EXTRACT, checks the numbers of lines (X and Y dimensions)
                if ((m_Method == EXTRACT) && ((m_NbLinesX == 0) || (m_NbLinesY == 0)))
                {
                    vnsExceptionBusinessMacro(
                            "Impossible to compute a extract image with an invalid extract size. The extract size has to be different than 0");
                }
            }

        }

    /** Generate */
    template<class TInputImage, class TOutputImage>
        void
        L1QuicklookTool<TInputImage, TOutputImage>::Generate(void)
        {
            this->CheckInputs();
            vnsLogDebugMacro("Start L1QuicklookTool ...")

            InputImageConstPointer inputImage = this->GetTOAInputImage();

            //create min/max pixel for rescaling
            InputImagePixelType inputMin;
            inputMin.SetSize(3);
            inputMin[0] = m_MinReflBRed;
            inputMin[1] = m_MinReflBGreen;
            inputMin[2] = m_MinReflBBlue;
            InputImagePixelType inputMax;
            inputMax.SetSize(3);
            inputMax[0] = m_MaxReflBRed;
            inputMax[1] = m_MaxReflBGreen;
            inputMax[2] = m_MaxReflBBlue;

            OutputImagePixelType outputMin;
            outputMin.SetSize(3);
            outputMin.Fill(m_OutputMin);
            OutputImagePixelType outputMax;
            outputMax.SetSize(3);
            outputMax.Fill(m_OutputMax);

            // Extract the wanted channels
            m_MultiChannelExtractROIFilter->SetInput(inputImage);
            m_MultiChannelExtractROIFilter->ClearChannels();
            m_MultiChannelExtractROIFilter->SetChannel(m_QLRedBand);
            m_MultiChannelExtractROIFilter->SetChannel(m_QLGreenBand);
            m_MultiChannelExtractROIFilter->SetChannel(m_QLBlueBand);

            //create noData pixel (variableLengthVector)
            InputImagePixelType noDataPixel;
            noDataPixel.SetSize(3);
            noDataPixel.Fill(m_NoData);
            InputImagePixelType pixelNoDataReplace;
            pixelNoDataReplace.SetSize(3);

            pixelNoDataReplace.Fill(m_NoDataReplaceValue);

            // Change no data value
            m_ChangeValueVectorImageFilter->SetInput(m_MultiChannelExtractROIFilter->GetOutput());
            m_ChangeValueVectorImageFilter->GetFunctor().SetNumberOfComponentsPerPixel(3);
            m_ChangeValueVectorImageFilter->GetFunctor().SetChange(noDataPixel, inputMin);

            // If under sampling asked
            if (m_Method == QUICKLOOK)
            {
                m_QuicklookGenerator->SetInput(m_ChangeValueVectorImageFilter->GetOutput());
                m_QuicklookGenerator->SetSubsampleFactor(m_Ratio);

                m_VectorRescaleIntensityImageFilter->SetInput(m_QuicklookGenerator->GetOutput());
            }
            // Else make an extract
            else
            {
                if ((m_NbLinesX != 0) && (m_NbLinesY != 0))
                {
                    if ((m_ULIndexX < 1) || (m_ULIndexY < 1))
                    {
                        vnsExceptionBusinessMacro("The Upper Left corner coordinates must be greater than 0 (ex: [1...]).")
                    }
                    m_MultiChannelExtractROIFilter->SetStartX(m_ULIndexX - 1); // - 1 because m_ULIndexX start at 1, and OTB at 0
                    m_MultiChannelExtractROIFilter->SetStartY(m_ULIndexY - 1); // - 1 because m_ULIndexX start at 1, and OTB at 0
                    m_MultiChannelExtractROIFilter->SetSizeX(m_NbLinesX);
                    m_MultiChannelExtractROIFilter->SetSizeY(m_NbLinesY);

                    m_VectorRescaleIntensityImageFilter->SetInput(m_ChangeValueVectorImageFilter->GetOutput());

                }
                else
                {
                    vnsExceptionBusinessMacro("Invalid extract size or ratio");
                }
            }

            vnsLogDebugMacro("Threshold before quicklook:")
            vnsLogDebugMacro("   inputMin :"<<inputMin)
            vnsLogDebugMacro("   inputMax :"<<inputMax)
            vnsLogDebugMacro(
                    "   outputMin:"<<static_cast<int>(outputMin[0])<<";"<<static_cast<int>(outputMin[1])<<";"<<static_cast<int>(outputMin[2]))
            vnsLogDebugMacro("   outputMax:"<<(int)outputMax[0]<<";"<<(int)outputMax[1]<<";"<<(int)outputMax[2])

            // Rescale the output
            m_VectorRescaleIntensityImageFilter->AutomaticInputMinMaxComputationOff();
            m_VectorRescaleIntensityImageFilter->SetOutputMaximum(outputMax);
            m_VectorRescaleIntensityImageFilter->SetOutputMinimum(outputMin);
            //inputMin.SetSize(3);
            //inputMin.Fill(0);
            m_VectorRescaleIntensityImageFilter->SetInputMaximum(inputMax);
            m_VectorRescaleIntensityImageFilter->SetInputMinimum(inputMin);

            //vnsCachingMacro(m_VectorRescaleIntensityImageFilterReader, OutputImageType, m_VectorRescaleIntensityImageFilter->GetOutput(),
                    "QL2_VectorRescaleIntensityImageFilter")

            // Then, to avoid having too small streams with the WriteTxtOnImageFilter
            // which would generate errors, the filter PrintSHDFilter is updated
            // to set the complete data in memory
            m_VectorRescaleIntensityImageFilter->Update();

            // Write or not the text in the output image
            if (m_WriteTxtOnImageFilter->GetFontSize() == 0)
            {
                m_OutputImage = m_VectorRescaleIntensityImageFilter->GetOutput();
            }
            else
            {
                // Add the text in the upper right image corner
                m_WriteTxtOnImageFilter->SetInput(m_VectorRescaleIntensityImageFilter->GetOutput());
                m_WriteTxtOnImageFilter->UpdateData();
                m_OutputImage = m_WriteTxtOnImageFilter->GetOutput();
            }
        }

    /** PrintSelf method */
    template<class TInputImage, class TOutputImage>
        void
        L1QuicklookTool<TInputImage, TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL1QuicklookTool_txx */

// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 07 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0-3 : FA : 236 : 14 septembre 2011 : Ajout de VAPNoData et AOTNoData.                        *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QuicklookTool_h
#define __vnsL2QuicklookTool_h

#include "itkProcessObject.h"
#include "vnsL2QLCompositeVectorImageFilter.h"
#include "vnsL2QLCompositeImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "itkAddImageFilter.h"
#include "vnsL2QLExtractEdge.h"
#include "otbImageFileReader.h"
#include "vnsMacro.h"

namespace vns
{
    /** \class  L2QuicklookTool
     * \brief This class implements ROI extraction and quicklook generation for L2 data
     *
     *
     *
     * \author CS Systemes d'Information
     *
     * \sa ImageToImageFilter
     *
     * \ingroup L2
     * \ingroup L3
     * \ingroup Checktool
     *
     */
    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        class L2QuicklookTool : public itk::ProcessObject
        {
        public:
            /** Standard class typedefs. */
            typedef L2QuicklookTool Self;
            typedef itk::ProcessObject Superclass;
            typedef itk::SmartPointer<Self> Pointer;
            typedef itk::SmartPointer<const Self> ConstPointer;

            /** Type macro */
            itkNewMacro(Self)

            /** Creation through object factory macro */
            itkTypeMacro(L2QuicklookTool, ProcessObject )

            /** Some convenient typedefs. */
            typedef TReflectanceImage InputReflectanceImageType;
            typedef typename InputReflectanceImageType::ConstPointer InputReflectanceImageConstPointer;
            typedef typename InputReflectanceImageType::Pointer InputReflectanceImagePointer;
            typedef typename InputReflectanceImageType::RegionType RegionType;
            typedef typename InputReflectanceImageType::PixelType InputReflectanceImagePixelType;
            typedef typename InputReflectanceImageType::InternalPixelType InputReflectanceImageInternalPixelType;
            typedef typename InputReflectanceImageType::SizeType SizeType;
            typedef typename InputReflectanceImageType::IndexType IndexType;

            typedef TOutputImage OutputImageType;
            typedef typename OutputImageType::Pointer OutputImagePointer;
            typedef typename OutputImageType::PixelType OutputImagePixelType;
            typedef typename OutputImageType::InternalPixelType OutputImageInternalPixelType;

            typedef TMask MaskImageType;
            typedef typename MaskImageType::ConstPointer MaskImageConstPointer;
            typedef typename MaskImageType::Pointer MaskImagePointer;
            typedef typename MaskImageType::InternalPixelType MaskImageInternalPixelType;
            typedef TVectorMask MaskVectorImageType;
            typedef typename MaskVectorImageType::ConstPointer MaskVectorImageConstPointer;
            typedef typename MaskVectorImageType::Pointer MaskVectorImagePointer;
            typedef typename MaskVectorImageType::InternalPixelType MaskVectorImageInternalPixelType;
            typedef TAmountImage InputAmountImageType;
            typedef typename InputAmountImageType::ConstPointer InputAmountImageConstPointer;
            typedef typename InputAmountImageType::Pointer InputAmountImagePointer;
            typedef typename InputAmountImageType::InternalPixelType InputAmountImageInternalPixelType;

            typedef L2QLExtractEdge<MaskImageType, MaskImageType> L2QLExtractEdgeFilterType;
            typedef L2QLCompositeVectorImageFilter<InputReflectanceImageType, MaskImageType, OutputImageType> ReflectanceL2QLCompositeFilterType;
            typedef L2QLCompositeImageFilter<InputAmountImageType, MaskImageType, OutputImageType> AmountL2QLCompositeFilterType;

            typedef otb::MultiToMonoChannelExtractROI<MaskVectorImageInternalPixelType, MaskImageInternalPixelType> MultiChannelExtractROIFilterType;
            typedef itk::AddImageFilter<MaskImageType, MaskImageType, MaskImageType> AddImageFilterType;

            typedef otb::ImageFileReader<MaskImageType> ReaderType;

            typedef typename L2QLExtractEdgeFilterType::Pointer L2QLExtractEdgeFilterPointerType;
            typedef typename ReflectanceL2QLCompositeFilterType::Pointer ReflectanceL2QLCompositeFilterPointerType;
            typedef typename AmountL2QLCompositeFilterType::Pointer AmountL2QLCompositeFilterPointerType;
            typedef typename MultiChannelExtractROIFilterType::Pointer MultiChannelExtractROIFilterPointerType;
            typedef typename AddImageFilterType::Pointer AddImageFilterPointerType;
            typedef typename ReaderType::Pointer ReaderPointerType;

            //input images accessors

            vnsSetGetImageMacro( L2VAPInput, InputAmountImageType, 0)
            vnsSetGetImageMacro( L2AOTInput, InputAmountImageType, 1)
            vnsSetGetImageMacro( L2SREInput, InputReflectanceImageType, 2)
            vnsSetGetImageMacro( L2FREInput, InputReflectanceImageType, 3)
            vnsSetGetImageMacro( L2CLDInput, MaskVectorImageType, 4)
            vnsSetGetImageMacro( L2WATInput, MaskImageType, 5)
            vnsSetGetImageMacro( L2SNWInput, MaskImageType, 6)

            //output QuickLook accessors
            OutputImageType*
            GetVAPOutput()
            {
                return m_VAPCompositeFilter->GetOutput();
            }
            OutputImageType*
            GetAOTOutput()
            {
                return m_AOTCompositeFilter->GetOutput();
            }
            OutputImageType*
            GetSREOutput()
            {
                return m_SRECompositeFilter->GetOutput();
            }
            OutputImageType*
            GetFREOutput()
            {
                if (m_UseFRE == true)
                {
                    return m_FRECompositeFilter->GetOutput();
                }
                else
                {
                    vnsExceptionBusinessMacro("FRE output not available.");
                }
            }

            //GIPP Glob accessors
            vnsUIntMemberAndGetConstReferenceMacro(Ratio)
            vnsUIntMemberAndGetConstReferenceMacro(QLRedBand)
            vnsUIntMemberAndGetConstReferenceMacro(QLGreenBand)
            vnsUIntMemberAndGetConstReferenceMacro(QLBlueBand)

            vnsMemberAndSetAndGetConstReferenceMacro(MinReflBRed , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MinReflBGreen , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MinReflBBlue , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MaxReflBRed , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MaxReflBGreen , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MaxReflBBlue , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(NoData , InputReflectanceImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(VAPNoData , InputAmountImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(AOTNoData , InputAmountImageInternalPixelType)

            vnsMemberAndSetAndGetConstReferenceMacro(NoDataReplaceValue , OutputImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(CloudLabel , OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(ShadowLabel , OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(WaterLabel , OutputImagePixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(SnowLabel , OutputImagePixelType)

            //GIPP L2
            vnsUIntMemberAndGetConstReferenceMacro(WaterVaporRatio)
            vnsUIntMemberAndGetConstReferenceMacro(AOTRatio)

            vnsMemberAndSetAndGetConstReferenceMacro(MinWaterVapor , InputAmountImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MaxWaterVapor , InputAmountImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MinAOT , InputAmountImageInternalPixelType)
            vnsMemberAndSetAndGetConstReferenceMacro(MaxAOT , InputAmountImageInternalPixelType)

            vnsUIntMemberAndGetConstReferenceMacro(ShadowBand1)
            vnsUIntMemberAndGetConstReferenceMacro(ShadowBand2)
            vnsUIntMemberAndGetConstReferenceMacro(CloudBand)
            vnsStringMemberAndSetAndGetConstReferenceMacro(Date)
            vnsUIntMemberAndGetConstReferenceMacro(FontSize)
            vnsStringMemberAndSetAndGetConstReferenceMacro(FontFileName)

            vnsBooleanMemberAndSetAndGetConstReferenceMacro(UseFRE)

            void
            SetTextColor(int redVal, int greenVal, int blueVal)
            {
                m_FRECompositeFilter->SetTextColor(redVal, greenVal, blueVal);
                m_SRECompositeFilter->SetTextColor(redVal, greenVal, blueVal);
                m_VAPCompositeFilter->SetTextColor(redVal, greenVal, blueVal);
                m_AOTCompositeFilter->SetTextColor(redVal, greenVal, blueVal);
            }

            void
            Generate();

        protected:
            /** Constructor */
            L2QuicklookTool();
            /** Destructor */
            virtual
            ~L2QuicklookTool();

            void
            CheckInputs();

            /** PrintSelf method */
            virtual void
            PrintSelf(std::ostream& os, itk::Indent indent) const;

        private:
            L2QuicklookTool(const Self&); //purposely not implemented
            void
            operator=(const Self&); //purposely not implemented

            L2QLExtractEdgeFilterPointerType m_CLDExtractEdgeFilter;
            L2QLExtractEdgeFilterPointerType m_WATExtractEdgeFilter;
            L2QLExtractEdgeFilterPointerType m_SHDExtractEdgeFilter;

            AmountL2QLCompositeFilterPointerType m_VAPCompositeFilter;
            AmountL2QLCompositeFilterPointerType m_AOTCompositeFilter;
            ReflectanceL2QLCompositeFilterPointerType m_SRECompositeFilter;
            ReflectanceL2QLCompositeFilterPointerType m_FRECompositeFilter;

            MultiChannelExtractROIFilterPointerType m_SHD1MultiChannelExtractROIFilter;
            MultiChannelExtractROIFilterPointerType m_SHD2MultiChannelExtractROIFilter;
            MultiChannelExtractROIFilterPointerType m_CLDMultiChannelExtractROIFilter;
            AddImageFilterPointerType m_BinaryAndSHDFilter;

            ReaderPointerType m_SHDCachReader;
            ReaderPointerType m_WATCachReader;
            ReaderPointerType m_CLDCachReader;

        };

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsL2QuicklookTool.txx"
#endif

#endif /* __vnsL2QuicklookTool_h */

// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 1-0-0-3 : FA : 236 : 14 septembre 2011 : Ajout de VAPNoData et AOTNoData.                        *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QuicklookTool_txx
#define __vnsL2QuicklookTool_txx

#include "vnsL2QuicklookTool.h"

namespace vns
{

    /** Constructor */
    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        L2QuicklookTool<TReflectanceImage, TAmountImage, TVectorMask, TMask, TOutputImage>::L2QuicklookTool() :
                m_Ratio(1), m_QLRedBand(0), m_QLGreenBand(0), m_QLBlueBand(0), m_MinReflBRed(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MinReflBGreen(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MinReflBBlue(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBRed(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBGreen(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBBlue(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_NoData(
                        static_cast<InputReflectanceImageInternalPixelType>(-10000)), m_VAPNoData(
                        static_cast<InputAmountImageInternalPixelType>(0)), m_AOTNoData(static_cast<InputAmountImageInternalPixelType>(0)), m_NoDataReplaceValue(
                        static_cast<OutputImageInternalPixelType>(0)), m_WaterVaporRatio(1), m_AOTRatio(1), m_MinWaterVapor(
                        static_cast<InputAmountImageInternalPixelType>(0)), m_MaxWaterVapor(
                        static_cast<InputAmountImageInternalPixelType>(0)), m_MinAOT(static_cast<InputAmountImageInternalPixelType>(0)), m_MaxAOT(
                        static_cast<InputAmountImageInternalPixelType>(0)), m_ShadowBand1(7), m_ShadowBand2(8), m_CloudBand(2), m_Date(""), m_FontSize(
                        0), m_FontFileName(""), m_UseFRE(true)
        {
            /** set number of required inputs */
            this->SetNumberOfRequiredInputs(6);
            this->SetNumberOfIndexedInputs(7);

            // shadow in black
            m_ShadowLabel.SetSize(3);
            m_ShadowLabel.Fill(0);

            // cloud in red
            m_CloudLabel.SetSize(3);
            m_CloudLabel.Fill(0);
            m_CloudLabel[0] = 255;

            //water in cyan
            m_WaterLabel.SetSize(3);
            m_WaterLabel[0] = 255;
            m_WaterLabel[1] = 255;
            m_WaterLabel[2] = 0;

            // snow in white
            m_SnowLabel.SetSize(3);
            m_SnowLabel.Fill(255);

            /** create filters */
            m_VAPCompositeFilter = AmountL2QLCompositeFilterType::New();
            m_AOTCompositeFilter = AmountL2QLCompositeFilterType::New();
            m_SRECompositeFilter = ReflectanceL2QLCompositeFilterType::New();
            m_FRECompositeFilter = ReflectanceL2QLCompositeFilterType::New();

            m_SHD1MultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_SHD2MultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_CLDMultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_BinaryAndSHDFilter = AddImageFilterType::New();

            m_SHDCachReader = ReaderType::New();
            m_CLDCachReader = ReaderType::New();
            m_WATCachReader = ReaderType::New();

        }

    /** Destructor */
    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        L2QuicklookTool<TReflectanceImage, TAmountImage, TVectorMask, TMask, TOutputImage>::~L2QuicklookTool()
        {
        }

    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        void
        L2QuicklookTool<TReflectanceImage, TAmountImage, TVectorMask, TMask, TOutputImage>::CheckInputs()
        {
            // Load inputs
            InputAmountImageConstPointer vapImage = this->GetL2VAPInputImage();
            InputAmountImageConstPointer aotImage = this->GetL2AOTInputImage();
            InputReflectanceImageConstPointer sreImage = this->GetL2SREInputImage();
            InputReflectanceImageConstPointer freImage = this->GetL2FREInputImage();
            MaskImageConstPointer watImage = this->GetL2WATInputImage();
            MaskImageConstPointer snwImage = this->GetL2SNWInputImage();
            MaskVectorImageConstPointer cldImage = this->GetL2CLDInputImage();

            // Check if each inputs have been set
            // L2 VAP input
            if (vapImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input VAP image.");
            }
            // L2 AOT input
            if (aotImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input AOT image.");
            }
            // L2 SRE input
            if (sreImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input SRE image.");
            }
            // L2 FRE input
            if ((freImage.IsNull() == true) && (m_UseFRE == true))
            {
                vnsExceptionBusinessMacro("Invalid input FRE image.");
            }
            // L2 WAT input
            if (watImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input WAT image.");
            }
            // L2 CLD input
            if (cldImage.IsNull() == true)
            {
                vnsExceptionBusinessMacro("Invalid input CLD image.");
            }

            // Check input ratio
            if (m_Ratio == 0)
            {
                vnsExceptionBusinessMacro("Invalid ratio. Ratio can't be null.");
            }
            if (m_AOTRatio == 0)
            {
                vnsExceptionBusinessMacro("Invalid AOT ratio. Ratio can't be null.");
            }

            if (m_WaterVaporRatio == 0)
            {
                vnsExceptionBusinessMacro("Invalid Water Vapor ratio. Ratio can't be null.");
            }

        }

    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        void
        L2QuicklookTool<TReflectanceImage, TAmountImage, TVectorMask, TMask, TOutputImage>::Generate()
        {
            this->CheckInputs();

            //Get Input Images
            InputAmountImageConstPointer vapImage = this->GetL2VAPInputImage();
            InputAmountImageConstPointer aotImage = this->GetL2AOTInputImage();
            InputReflectanceImageConstPointer sreImage = this->GetL2SREInputImage();
            InputReflectanceImageConstPointer freImage = this->GetL2FREInputImage();
            MaskImageConstPointer watImage = this->GetL2WATInputImage();
            MaskImageConstPointer snwImage = this->GetL2SNWInputImage();
            MaskVectorImageConstPointer cldImage = this->GetL2CLDInputImage();

            //Get SHD1 Output images
            m_SHD1MultiChannelExtractROIFilter->SetInput(cldImage);
            m_SHD1MultiChannelExtractROIFilter->SetChannel(m_ShadowBand1);
            m_SHD1MultiChannelExtractROIFilter->UpdateOutputInformation();

            //Get SHD2 Output images
            m_SHD2MultiChannelExtractROIFilter->SetInput(cldImage);
            m_SHD2MultiChannelExtractROIFilter->SetChannel(m_ShadowBand2);

            //Get CLD Output images
            m_CLDMultiChannelExtractROIFilter->SetInput(cldImage);
            m_CLDMultiChannelExtractROIFilter->SetChannel(m_CloudBand);

            vnsLogDebugMacro("m_ShadowBand1: "<<m_ShadowBand1)
            vnsLogDebugMacro("m_ShadowBand2: "<<m_ShadowBand2)
            vnsLogDebugMacro("m_CloudBand:   "<<m_CloudBand)
            m_BinaryAndSHDFilter->SetInput1(m_SHD1MultiChannelExtractROIFilter->GetOutput());
            m_BinaryAndSHDFilter->SetInput2(m_SHD2MultiChannelExtractROIFilter->GetOutput());

            //Create Caching
            //vnsCachingMacro(m_CLDCachReader, MaskImageType,
            //        m_CLDMultiChannelExtractROIFilter->GetOutput(), "CLDMultiChannelExtractROIFilter");
            //vnsCachingMacro(m_SHDCachReader, MaskImageType, m_BinaryAndSHDFilter->GetOutput(),
            //        "BinaryAndSHDFilter");
            
            //--------------------------------------------------------------
            //Initialize the AOT Composite filter
            // Set all parameters
            //--------------------------------------------------------------           
            m_AOTCompositeFilter->SetInputImage(aotImage);
            m_AOTCompositeFilter->SetInputWATMask(watImage);
            m_AOTCompositeFilter->SetInputSNWMask(snwImage);
            m_AOTCompositeFilter->SetInputCLDMask(m_CLDMultiChannelExtractROIFilter->GetOutput());
            m_AOTCompositeFilter->SetInputSHDMask(m_BinaryAndSHDFilter->GetOutput());
            m_AOTCompositeFilter->SetMinValue(m_MinAOT);
            m_AOTCompositeFilter->SetMaxValue(m_MaxAOT);
            m_AOTCompositeFilter->SetRatio(m_AOTRatio);
            m_AOTCompositeFilter->SetNoData(m_AOTNoData);
            m_AOTCompositeFilter->SetNoDataReplaceValue(m_NoDataReplaceValue);
            m_AOTCompositeFilter->SetCloudLabel(m_CloudLabel);
            m_AOTCompositeFilter->SetShadowLabel(m_ShadowLabel);
            m_AOTCompositeFilter->SetWaterLabel(m_WaterLabel);
            m_AOTCompositeFilter->SetSnowLabel(m_SnowLabel);
            m_AOTCompositeFilter->SetText(m_Date + "\nAOT");
            m_AOTCompositeFilter->SetFontSize(m_FontSize);
            m_AOTCompositeFilter->SetFontFileName(m_FontFileName);
            
            //--------------------------------------------------------------
            // Launch processing
            m_AOTCompositeFilter->UpdateData();

            //--------------------------------------------------------------
            // Initialize the VAP Composite filter
            // Set all parameters
            //--------------------------------------------------------------
            m_VAPCompositeFilter->SetInputImage(vapImage);
            m_VAPCompositeFilter->SetInputWATMask(watImage);
            m_VAPCompositeFilter->SetInputSNWMask(snwImage);
            m_VAPCompositeFilter->SetInputCLDMask(m_CLDMultiChannelExtractROIFilter->GetOutput());
            m_VAPCompositeFilter->SetInputSHDMask(m_BinaryAndSHDFilter->GetOutput());
            m_VAPCompositeFilter->SetMinValue(m_MinWaterVapor);
            m_VAPCompositeFilter->SetMaxValue(m_MaxWaterVapor);
            m_VAPCompositeFilter->SetRatio(m_WaterVaporRatio);
            m_VAPCompositeFilter->SetNoData(m_VAPNoData);
            m_VAPCompositeFilter->SetNoDataReplaceValue(m_NoDataReplaceValue);
            m_VAPCompositeFilter->SetCloudLabel(m_CloudLabel);
            m_VAPCompositeFilter->SetShadowLabel(m_ShadowLabel);
            m_VAPCompositeFilter->SetSnowLabel(m_SnowLabel);
            m_VAPCompositeFilter->SetWaterLabel(m_WaterLabel);
            m_VAPCompositeFilter->SetText(m_Date + "\nVAP");
            m_VAPCompositeFilter->SetFontSize(m_FontSize);
            m_VAPCompositeFilter->SetFontFileName(m_FontFileName);

            // Launch processing
            m_VAPCompositeFilter->UpdateData();

            //create noData pixel and noDataReplaceValue(variableLengthVector)
            InputReflectanceImagePixelType noDataPixel;
            noDataPixel.SetSize(3);
            noDataPixel.Fill(m_NoData);
            //Init no data replace value
            OutputImagePixelType noDataReplacePixel;
            noDataReplacePixel.SetSize(3);
            noDataReplacePixel.Fill(m_NoDataReplaceValue);

            //--------------------------------------------------------------
            //Initialize the SRE Composite filter
            // Set all parameters
            //--------------------------------------------------------------
            m_SRECompositeFilter->SetInputImage(sreImage);
            m_SRECompositeFilter->SetInputWATMask(watImage);
            m_SRECompositeFilter->SetInputSNWMask(snwImage);
            m_SRECompositeFilter->SetInputCLDMask(m_CLDMultiChannelExtractROIFilter->GetOutput());
            m_SRECompositeFilter->SetInputSHDMask(m_BinaryAndSHDFilter->GetOutput());
            m_SRECompositeFilter->SetQLRedBand(m_QLRedBand);
            m_SRECompositeFilter->SetQLGreenBand(m_QLGreenBand);
            m_SRECompositeFilter->SetQLBlueBand(m_QLBlueBand);
            m_SRECompositeFilter->SetMinValueBRed(m_MinReflBRed);
            m_SRECompositeFilter->SetMaxValueBRed(m_MaxReflBRed);
            m_SRECompositeFilter->SetMinValueBGreen(m_MinReflBGreen);
            m_SRECompositeFilter->SetMaxValueBGreen(m_MaxReflBGreen);
            m_SRECompositeFilter->SetMinValueBBlue(m_MinReflBBlue);
            m_SRECompositeFilter->SetMaxValueBBlue(m_MaxReflBBlue);
            m_SRECompositeFilter->SetRatio(m_Ratio);
            m_SRECompositeFilter->SetNoData(m_NoData);
            m_SRECompositeFilter->SetNoDataReplaceValue(m_NoDataReplaceValue);
            m_SRECompositeFilter->SetCloudLabel(m_CloudLabel);
            m_SRECompositeFilter->SetShadowLabel(m_ShadowLabel);
            m_SRECompositeFilter->SetWaterLabel(m_WaterLabel);
            m_SRECompositeFilter->SetSnowLabel(m_SnowLabel);
            m_SRECompositeFilter->SetText(m_Date + "\nSRE");
            m_SRECompositeFilter->SetFontSize(m_FontSize);
            m_SRECompositeFilter->SetFontFileName(m_FontFileName);

            //--------------------------------------------------------------
            // Launch processing
            m_SRECompositeFilter->UpdateData();

            //--------------------------------------------------------------
            // If FRE image is available
            if (m_UseFRE == true)
            {
                //--------------------------------------------------------------
                //Initialize the FRE Composite filter
                // Set all parameters
                //--------------------------------------------------------------
                m_FRECompositeFilter->SetInputImage(freImage);
                m_FRECompositeFilter->SetInputWATMask(watImage);
                m_FRECompositeFilter->SetInputSNWMask(snwImage);
                m_FRECompositeFilter->SetInputCLDMask(m_CLDMultiChannelExtractROIFilter->GetOutput());
                m_FRECompositeFilter->SetInputSHDMask(m_BinaryAndSHDFilter->GetOutput());
                m_FRECompositeFilter->SetQLRedBand(m_QLRedBand);
                m_FRECompositeFilter->SetQLGreenBand(m_QLGreenBand);
                m_FRECompositeFilter->SetQLBlueBand(m_QLBlueBand);
                m_FRECompositeFilter->SetMinValueBRed(m_MinReflBRed);
                m_FRECompositeFilter->SetMaxValueBRed(m_MaxReflBRed);
                m_FRECompositeFilter->SetMinValueBGreen(m_MinReflBGreen);
                m_FRECompositeFilter->SetMaxValueBGreen(m_MaxReflBGreen);
                m_FRECompositeFilter->SetMinValueBBlue(m_MinReflBBlue);
                m_FRECompositeFilter->SetMaxValueBBlue(m_MaxReflBBlue);
                m_FRECompositeFilter->SetRatio(m_Ratio);
                m_FRECompositeFilter->SetNoData(m_NoData);
                m_FRECompositeFilter->SetNoDataReplaceValue(m_NoDataReplaceValue);
                m_FRECompositeFilter->SetCloudLabel(m_CloudLabel);
                m_FRECompositeFilter->SetShadowLabel(m_ShadowLabel);
                m_FRECompositeFilter->SetWaterLabel(m_WaterLabel);
                m_FRECompositeFilter->SetSnowLabel(m_SnowLabel);
                m_FRECompositeFilter->SetText(m_Date + "\nFRE");
                m_FRECompositeFilter->SetFontSize(m_FontSize);
                m_FRECompositeFilter->SetFontFileName(m_FontFileName);

                //--------------------------------------------------------------
                // Launch processing
                m_FRECompositeFilter->UpdateData();
            }
            // No FRE band
            else
            {
                vnsLogWarningMacro("Use of FRE image disabled.")
            }


            
        }

    /** PrintSelf method */
    template<class TReflectanceImage, class TAmountImage, class TVectorMask, class TMask, class TOutputImage>
        void
        L2QuicklookTool<TReflectanceImage, TAmountImage, TVectorMask, TMask, TOutputImage>::PrintSelf(std::ostream& os,
                itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL2QuicklookTool_txx */

// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153436-CS : 13 avril 2017 : Classe ThesholdVector dispo dans l'otb   *
 * VERSION : 5-1-0 : FA : LAIG-FA-MAC-145739-CS : 27 juin 2016 : Audit code - Supp de la macro ITK_EXPORT   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 27 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsWriteTxtOnImageFilter_h
#define __vnsWriteTxtOnImageFilter_h

#include "itkProcessObject.h"
#include "vnsPasteImageFilter.h"
#include "vnsConvertTxtToImageFilter.h"
#include "otbThresholdVectorImageFilter.h"

namespace vns
{
/** \class  WriteTxtOnImageFilter
 * \brief This filter writes an std::string into a VectorImage
 *
 * ATTENTION: Filter have not streaming capabilities
 * ATTENTION: Then, to avoid having too small streams with the WriteTxtOnImageFilter
 * ATTENTION: which would generate errors, the input must be updated
 * ATTENTION: to set the complete data in memory
 *
 * \author CS Systemes d'Information
 *
 * \sa ImageToImageFilter
 *
 * \ingroup L2
 * \ingroup L3
 * \ingroup Checktool
 *
 */
template <class TInputImage, class TOutputImage>
  class WriteTxtOnImageFilter : public itk::ProcessObject
{
public:
    /** Standard class typedefs. */
    typedef WriteTxtOnImageFilter                                  Self;
    typedef itk::ProcessObject                                     Superclass;
    typedef itk::SmartPointer<Self>                                Pointer;
    typedef itk::SmartPointer<const Self>                          ConstPointer;

    /** Type macro */
    itkNewMacro(Self);
    /** Creation through object factory macro */
    itkTypeMacro(WriteTxtOnImageFilter, ImageToImageFilter );

    /** Some convenient typedefs. */
    typedef TInputImage                           InputImageType;
    typedef typename InputImageType::ConstPointer InputImageConstPointer;
    typedef typename InputImageType::Pointer      InputImagePointer;
    typedef typename InputImageType::RegionType   RegionType;
    typedef typename InputImageType::PixelType    InputImagePixelType;
    typedef typename InputImageType::SizeType     SizeType;
    typedef typename InputImageType::IndexType    IndexType;
    typedef TOutputImage                          OutputImageType;
    typedef typename OutputImageType::Pointer     OutputImagePointer;
    typedef typename OutputImageType::PixelType   OutputImagePixelType;

    typedef ConvertTxtToImageFilter<OutputImageType> ConvertTxtToImageFilterType;
    typedef PasteImageFilter<InputImageType,OutputImageType,OutputImageType> PasteImageFilterType;
    typedef otb::ThresholdVectorImageFilter<OutputImageType> ThresholdFilterType;

    typedef typename ConvertTxtToImageFilterType::Pointer ConvertTxtToImageFilterPointerType;
    typedef typename PasteImageFilterType::Pointer PasteImageFilterPointerType;
    typedef typename ThresholdFilterType::Pointer ThresholdFilterPointerType;

    /** Destination Index : where to write the text in the input image */
    itkSetMacro(DestinationIndex, IndexType);
    itkGetConstMacro(DestinationIndex, IndexType);

    /** Police size */
    itkSetMacro(FontSize, unsigned int);
    itkGetMacro(FontSize, unsigned int);

    /** String to convert into image */
    itkSetMacro(Text, std::string);
    itkGetMacro(Text, std::string);

    /** Set the Font file name */
    otbSetObjectMemberMacro(ConvertTxtToImageFilter,FontFileName, std::string)

    void SetTextColor(int redVal, int greenVal, int blueVal)
    {
      m_ConvertTxtToImageFilter->SetColor(redVal, greenVal, blueVal);
    }

    itkSetObjectMacro(Input, InputImageType);
    itkGetConstObjectMacro(Input, InputImageType);

    OutputImageType * GetOutput(void)
      {
        return m_PasteImageFilter->GetOutput();
      } 

    void UpdateData(void);

protected:
    /** Constructor */
    WriteTxtOnImageFilter();
    /** Destructor */
    virtual ~WriteTxtOnImageFilter();
    /** PrintSelf method */
    virtual void PrintSelf(std::ostream& /*os*/, itk::Indent /*indent*/) const{};

private:
    WriteTxtOnImageFilter(const Self&); //purposely not implemented
    void operator=(const Self&); //purposely not implemented

    IndexType m_DestinationIndex;
    unsigned int m_FontSize;
    std::string m_Text;

    ConvertTxtToImageFilterPointerType m_ConvertTxtToImageFilter;
    PasteImageFilterPointerType m_PasteImageFilter;
    InputImagePointer m_Input;
};

} // End namespace vns
#ifndef VNS_MANUAL_INSTANTIATION
#include "vnsWriteTxtOnImageFilter.txx"
#endif

#endif /* __vnsWriteTxtOnImageFilter_h */

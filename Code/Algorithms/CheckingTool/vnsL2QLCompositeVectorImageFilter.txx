// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 19 octobre 2016 : Audit code - Correction qualite         *
 * VERSION : 4-5-0 : FA : LAIG-FA-MAC-1386-CNES : 21 aout 2015 : Modification ordre ecriture contour  QL2   *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 31 août 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QLCompositeVectorImageFilter_txx
#define __vnsL2QLCompositeVectorImageFilter_txx

#include "vnsL2QLCompositeVectorImageFilter.h"
// #include "vnsCaching.h"
#include "vnsMacro.h"

namespace vns
{

    /** Constructor */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        L2QLCompositeVectorImageFilter<TInputImage, TMaskImage, TOutputImage>::L2QLCompositeVectorImageFilter() :
                m_BackgroundMaskValue(static_cast<MaskImageInternalPixelType>(0)), m_Ratio(1), m_QLRedBand(0), m_QLGreenBand(0), m_QLBlueBand(
                        0), m_MinValueBRed(static_cast<InputImageInternalPixelType>(0)), m_MinValueBGreen(
                        static_cast<InputImageInternalPixelType>(0)), m_MinValueBBlue(static_cast<InputImageInternalPixelType>(0)), m_MaxValueBRed(
                        static_cast<InputImageInternalPixelType>(0)), m_MaxValueBGreen(static_cast<InputImageInternalPixelType>(0)), m_MaxValueBBlue(
                        static_cast<InputImageInternalPixelType>(0)), m_NoData(static_cast<InputImageInternalPixelType>(-1000)), m_NoDataReplaceValue(
                        static_cast<OutputImageInternalPixelType>(0)), m_OutputMin(static_cast<OutputImageInternalPixelType>(0)), m_OutputMax(
                        static_cast<OutputImageInternalPixelType>(255))
        {
            /** set number of required inputs */
            this->SetNumberOfRequiredInputs(4);
            this->SetNumberOfIndexedInputs(5);

            // Shadow label
            m_ShadowLabel.SetSize(3);
            m_ShadowLabel.Fill(0);
            m_ShadowLabel[1] = 255;
            m_CloudLabel.SetSize(3);
            // Cloud label
            m_CloudLabel.Fill(0);
            m_CloudLabel[0] = 255;
            // Water Label
            m_WaterLabel.SetSize(3);
            m_WaterLabel.Fill(255);
            m_WaterLabel[0] = 0;
            // Snow Label
            m_SnowLabel.SetSize(3);
            m_SnowLabel.Fill(255);

            /** Create filters */
            m_MultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_ChangeValueVectorImageFilter = ChangeValueVectorImageFilterType::New();
            m_PrintCLDFilter = PrintableImageFilterType::New();
            m_PrintWATFilter = PrintableImageFilterType::New();
            m_PrintSHDFilter = PrintableImageFilterType::New();
            m_PrintSNWFilter = PrintableImageFilterType::New();
            m_QuicklookGenerator = QuicklookImageGeneratorType::New();
            m_VectorRescaleIntensityImageFilter = VectorRescaleIntensityImageFilterType::New();

            m_CloudQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_WaterQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_ShadowQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_SnowQuicklookGenerator = QuicklookMaskGeneratorType::New();

            m_CLDExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_WATExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_SHDExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_SNWExtractEdgeFilter = L2QLExtractEdgeFilterType::New();

            // m_PrintMaskReader = PrintSHDReaderType::New();

            m_WriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();
            m_OutputImage = OutputImageType::New();

        }

    /** Destructor */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        L2QLCompositeVectorImageFilter<TInputImage, TMaskImage, TOutputImage>::~L2QLCompositeVectorImageFilter()
        {
        }

    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeVectorImageFilter<TInputImage, TMaskImage, TOutputImage>::GenerateOutputInformation(void)
        {
            Superclass::GenerateOutputInformation();

            InputImageConstPointer inputPtr = this->GetInputImage();
            OutputImagePointer outputPtr = this->GetOutput();

            if (!outputPtr)
            {
                return;
            }

            m_QuicklookGenerator->SetSubsampleFactor(m_Ratio);
            m_QuicklookGenerator->UpdateOutputInformation();

            outputPtr->SetLargestPossibleRegion(m_QuicklookGenerator->GetOutput()->GetLargestPossibleRegion());
            outputPtr->SetNumberOfComponentsPerPixel(3);

        }

    /** UpdateData Composite method */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeVectorImageFilter<TInputImage, TMaskImage, TOutputImage>::UpdateData()
        {
            // Load inputs
            InputImageConstPointer inputImage = this->GetInputImage();
            MaskImageConstPointer inputWATMask = this->GetInputWATMask();
            MaskImageConstPointer inputCLDMask = this->GetInputCLDMask();
            MaskImageConstPointer inputSHDMask = this->GetInputSHDMask();
            MaskImageConstPointer inputSNWMask = this->GetInputSNWMask();

            // Generate mask quick look
            m_CloudQuicklookGenerator->SetInput(inputCLDMask);
            m_CloudQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            m_WaterQuicklookGenerator->SetInput(inputWATMask);
            m_WaterQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            m_ShadowQuicklookGenerator->SetInput(inputSHDMask);
            m_ShadowQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            // Generate mask edge
            m_CLDExtractEdgeFilter->SetInput(m_CloudQuicklookGenerator->GetOutput());
            m_WATExtractEdgeFilter->SetInput(m_WaterQuicklookGenerator->GetOutput());
            m_SHDExtractEdgeFilter->SetInput(m_ShadowQuicklookGenerator->GetOutput());

            m_CLDExtractEdgeFilter->UpdateData();
            m_WATExtractEdgeFilter->UpdateData();
            m_SHDExtractEdgeFilter->UpdateData();

            // Create min/max pixel for rescaling
            InputImagePixelType inputMin;
            inputMin.SetSize(3);
            inputMin[0] = m_MinValueBRed;
            inputMin[1] = m_MinValueBGreen;
            inputMin[2] = m_MinValueBBlue;
            InputImagePixelType inputMax;
            inputMax.SetSize(3);
            inputMax[0] = m_MaxValueBRed;
            inputMax[1] = m_MaxValueBGreen;
            inputMax[2] = m_MaxValueBBlue;
            OutputImagePixelType outputMin;
            outputMin.SetSize(3);
            outputMin.Fill(m_OutputMin);
            OutputImagePixelType outputMax;
            outputMax.SetSize(3);
            outputMax.Fill(m_OutputMax);

            // Extract band of the XS image
            m_MultiChannelExtractROIFilter->SetInput(inputImage);
            m_MultiChannelExtractROIFilter->ClearChannels();
            m_MultiChannelExtractROIFilter->SetChannel(m_QLRedBand);
            m_MultiChannelExtractROIFilter->SetChannel(m_QLGreenBand);
            m_MultiChannelExtractROIFilter->SetChannel(m_QLBlueBand);
            m_MultiChannelExtractROIFilter->UpdateOutputInformation();

            //create noData pixel and noDataReplaceValue(variableLengthVector)
            InputImagePixelType noDataPixel;
            noDataPixel.SetSize(3);
            noDataPixel.Fill(m_NoData);

            //compute input pixel value which give NoDataReplaceValue when rescale.
            InputImagePixelType noDataReplacePixel;
            noDataReplacePixel.SetSize(3);

            noDataReplacePixel.Fill(m_NoDataReplaceValue);

            // Change NoData value into noDataReplacePixel
            m_ChangeValueVectorImageFilter->SetInput(m_MultiChannelExtractROIFilter->GetOutput());
            m_ChangeValueVectorImageFilter->GetFunctor().SetNumberOfComponentsPerPixel(m_MultiChannelExtractROIFilter->GetNbChannels());
            m_ChangeValueVectorImageFilter->GetFunctor().SetChange(noDataPixel, inputMin);

            // Generate Quick look
            m_QuicklookGenerator->SetInput(m_ChangeValueVectorImageFilter->GetOutput());
            m_QuicklookGenerator->SetSubsampleFactor(m_Ratio);
            //m_ChangeValueVectorImageFilter->UpdateOutputInformation();
            //m_QuicklookGenerator->UpdateOutputInformation();

            // Rescale the quicklook
            m_VectorRescaleIntensityImageFilter->SetInput(m_QuicklookGenerator->GetOutput());
            m_VectorRescaleIntensityImageFilter->SetOutputMaximum(outputMax);
            m_VectorRescaleIntensityImageFilter->SetOutputMinimum(outputMin);
            m_VectorRescaleIntensityImageFilter->SetInputMaximum(inputMax);
            m_VectorRescaleIntensityImageFilter->SetInputMinimum(inputMin);
            m_VectorRescaleIntensityImageFilter->AutomaticInputMinMaxComputationOff();

            m_VectorRescaleIntensityImageFilter->UpdateOutputInformation();

            // 1. Add water
            m_PrintWATFilter->SetInput1(m_VectorRescaleIntensityImageFilter->GetOutput());
            m_PrintWATFilter->SetInput2(m_WATExtractEdgeFilter->GetOutput());
            m_PrintWATFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintWATFilter->GetFunctor().SetChange(m_WaterLabel);
            // vnsCachingTxxMacroWithReader(l_PrintWATFilterOutputReader, OutputImageType, m_PrintWATFilter->GetOutput(), "PrintWATFilter");

            // 2. Add shadow
            m_PrintSHDFilter->SetInput1(m_PrintWATFilter->GetOutput());
            m_PrintSHDFilter->SetInput2(m_SHDExtractEdgeFilter->GetOutput());
            m_PrintSHDFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintSHDFilter->GetFunctor().SetChange(m_ShadowLabel);
            // vnsCachingTxxMacroWithReader(l_PrintSHDFilterOutputReader, OutputImageType, m_PrintSHDFilter->GetOutput(), "PrintSHDFilter");

            // 3. Add cloud
            m_PrintCLDFilter->SetInput1(m_PrintSHDFilter->GetOutput());
            m_PrintCLDFilter->SetInput2(m_CLDExtractEdgeFilter->GetOutput());
            m_PrintCLDFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintCLDFilter->GetFunctor().SetChange(m_CloudLabel);

            // 4. Add Snow
            if (inputSNWMask.IsNull() == false)
            {
                m_SnowQuicklookGenerator->SetInput(inputSNWMask);
                m_SnowQuicklookGenerator->SetSubsampleFactor(m_Ratio);
                m_SNWExtractEdgeFilter->SetInput(m_SnowQuicklookGenerator->GetOutput());
                m_SNWExtractEdgeFilter->UpdateData();
                // vnsCachingTxxMacroWithReader(l_PrintCLDFilterOutputReader, OutputImageType, m_PrintCLDFilter->GetOutput(), "PrintSHDFilter");

                m_PrintSNWFilter->SetInput1(m_PrintCLDFilter->GetOutput());
                m_PrintSNWFilter->SetInput2(m_SNWExtractEdgeFilter->GetOutput());
                m_PrintSNWFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
                m_PrintSNWFilter->GetFunctor().SetChange(m_SnowLabel);
                m_PrintSNWFilter->Update() ;
               

                // Warning : the caching is used here to free the memory.
                // vnsCachingMacro(m_PrintMaskReader, OutputImageType, m_PrintSNWFilter->GetOutput(), "QL_L2PrintMaskFilter")

                // Then, to avoid having too small streams with the WriteTxtOnImageFilter
                // which would generate errors, the filter PrintSHDFilter is updated
                // to set the complete data in memory
                // m_PrintMaskReader->Update();
                m_PrintSNWFilter->Update() ;

                // Write or not the text in the output image
                if (m_WriteTxtOnImageFilter->GetFontSize() == 0)
                {
                    m_OutputImage = m_PrintSNWFilter->GetOutput();
                }
                else
                {
                    // Add the text in the upper right image corner
                    m_WriteTxtOnImageFilter->SetInput(m_PrintSNWFilter->GetOutput());
                    m_WriteTxtOnImageFilter->UpdateData();
                    m_OutputImage = m_WriteTxtOnImageFilter->GetOutput();
                }

            }
            else
            {
                // Warning : the caching is used here to free the memory.
                // vnsCachingMacro(m_PrintMaskReader, OutputImageType, m_PrintCLDFilter->GetOutput(), "QL_L2PrintMaskFilter")
                
                // Then, to avoid having too small streams with the WriteTxtOnImageFilter
                // which would generate errors, the filter PrintSHDFilter is updated
                // to set the complete data in memory
                // Write or not the text in the output image
                if (m_WriteTxtOnImageFilter->GetFontSize() == 0)
                {
                    m_OutputImage = m_PrintCLDFilter->GetOutput();
                }
                else
                {
                    // Add the text in the upper right image corner
                    m_WriteTxtOnImageFilter->SetInput(m_PrintCLDFilter->GetOutput());
                    m_WriteTxtOnImageFilter->UpdateData();
                    m_OutputImage = m_WriteTxtOnImageFilter->GetOutput();
                }

            }

        }

    /** PrintSelf method */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeVectorImageFilter<TInputImage, TMaskImage, TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL2QLCompositeVectorImageFilter_txx */

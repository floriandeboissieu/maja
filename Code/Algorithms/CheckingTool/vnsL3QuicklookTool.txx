// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 19 octobre 2016 : Audit code - Correction qualite         *
 * VERSION : 5-0-0 : FA : LAIG-FA-MAC-143764-CS : 28 avril 2016 : Mise en forme du code (indentation, etc.) *
 * VERSION : 4-7-3 : FA : LAIG-FA-MAC-141974-CS : 15 fevrier 2016 : Controle sur les bornes Min et Max pour *
 *                                                                 l' adaptation de la dynamique de l'image *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-117040-CS : 14 avril 2014 : Suppression du filtre non utilise         *
 *                                                                         m_PXDComputeRaisedBitsImageFilte *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 8 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL3QuicklookTool_txx
#define __vnsL3QuicklookTool_txx

#include "vnsL3QuicklookTool.h"
//#include "vnsCaching.h"

namespace vns
{

    /** Constructor */
    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        L3QuicklookTool<TInputReflectanceImage, TPXDTImage, TOutputImage>::L3QuicklookTool() :
                m_Ratio(1), m_QLRedBand(0), m_MinReflBRed(static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBRed(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_QLGreenBand(0), m_MinReflBGreen(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBGreen(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_QLBlueBand(0), m_MinReflBBlue(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_MaxReflBBlue(
                        static_cast<InputReflectanceImageInternalPixelType>(0)), m_NoData(
                        static_cast<InputReflectanceImageInternalPixelType>(-1000)), m_NoDataReplaceValue(
                        static_cast<OutputImageInternalPixelType>(0)), m_NbDate(0), m_OutputMin(0), m_OutputMax(255), m_Date(""), m_FontSize(
                        0), m_FontFileName(""), m_UseFRE(true)

        {
            // set number of required inputs
            this->SetNumberOfRequiredInputs(3);
            this->SetNumberOfIndexedInputs(3);

            // Create filters
            m_ScalarToRainbowVectorImageFilter = ScalarToRainbowVectorImageFilterType::New();
            m_PXDQuicklookGenerator = QuicklookPXDImageGeneratorType::New();
            m_PXDWriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();

            m_SREMultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_SREChangeValueVectorImageFilter = ChangeValueVectorImageFilterType::New();
            m_SREQuicklookGenerator = QuicklookReflectanceImageGeneratorType::New();
            m_SREVectorRescaleIntensityImageFilter = VectorRescaleIntensityImageFilterType::New();
            m_SREWriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();

            m_FREMultiChannelExtractROIFilter = MultiChannelExtractROIFilterType::New();
            m_FREChangeValueVectorImageFilter = ChangeValueVectorImageFilterType::New();
            m_FREQuicklookGenerator = QuicklookReflectanceImageGeneratorType::New();
            m_FREVectorRescaleIntensityImageFilter = VectorRescaleIntensityImageFilterType::New();
            m_FREWriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();

            m_SREVectorRescaleIntensityImageFilterReader = OutputImageReaderType::New();
            m_FREVectorRescaleIntensityImageFilterReader = OutputImageReaderType::New();
            m_ScalarToRainbowVectorImageFilterReader = OutputImageReaderType::New();

            m_PXDOutputImage = OutputImageType::New();
            m_FREOutputImage = OutputImageType::New();
            m_SREOutputImage = OutputImageType::New();

        }

    /** Destructor */
    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        L3QuicklookTool<TInputReflectanceImage, TPXDTImage, TOutputImage>::~L3QuicklookTool()
        {
        }

    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        void
        L3QuicklookTool<TInputReflectanceImage, TPXDTImage, TOutputImage>::CheckInputs(void)
        {
            // Check PXD Input pointer
            PXDInputImageConstPointer pixInputImage = this->GetPXDInputImage();
            if (pixInputImage.IsNull())
            {
                vnsExceptionBusinessMacro("PXD input is null");
            }

            // Check SRE Input pointer
            InputReflectanceImageConstPointer sreInputImage = this->GetSREInputImage();
            if (sreInputImage.IsNull())
            {
                vnsExceptionBusinessMacro("SRE input is null");
            }

            //Get FRE Input pointer
            if (m_UseFRE == true)
            {
                InputReflectanceImageConstPointer freInputImage = this->GetFREInputImage();
                if (freInputImage.IsNull() == true)
                {
                    vnsExceptionBusinessMacro("FRE input is null");
                }
            }
        }

    /** Generate Data */
    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        void
        L3QuicklookTool<TInputReflectanceImage, TPXDTImage, TOutputImage>::Generate()
        {
            this->CheckInputs();

            //Get Inputs pointer
            PXDInputImageConstPointer pixInputImage = this->GetPXDInputImage();
            InputReflectanceImageConstPointer sreInputImage = this->GetSREInputImage();
            //InputReflectanceImageConstPointer freInputImage = this->GetFREInputImage();

            vnsCrocodileClipMacro(PXDInputImageType, pixInputImage, "GetPXDInputImage.tif")

            //create min/max pixel for rescaling
            InputReflectanceImagePixelType inputMin;
            inputMin.SetSize(3);
            inputMin[0] = m_MinReflBRed;
            inputMin[1] = m_MinReflBGreen;
            inputMin[2] = m_MinReflBBlue;
            InputReflectanceImagePixelType inputMax;
            inputMax.SetSize(3);
            inputMax[0] = m_MaxReflBRed;
            inputMax[1] = m_MaxReflBGreen;
            inputMax[2] = m_MaxReflBBlue;
            OutputImagePixelType outputMin;
            outputMin.SetSize(3);
            outputMin.Fill(m_OutputMin);
            OutputImagePixelType outputMax;
            outputMax.SetSize(3);
            outputMax.Fill(m_OutputMax);

            //create noData pixel and noDataReplaceValue(variableLengthVector)
            InputReflectanceImagePixelType noDataPixel;
            noDataPixel.SetSize(3);
            noDataPixel.Fill(m_NoData);

            //compute input pixel value which give NoDataReplaceValue when rescale.
            InputReflectanceImagePixelType noDataReplacePixel;
            noDataReplacePixel.SetSize(3);
            noDataReplacePixel.Fill(m_NoDataReplaceValue);

            ////PXD output
            // PXD under sampling
            m_PXDQuicklookGenerator->SetInput(pixInputImage);
            m_PXDQuicklookGenerator->SetSubsampleFactor(m_Ratio);
            m_PXDQuicklookGenerator->UpdateOutputInformation();

            // Line commented in MACCS 4.0 because ComputeRaised filter not used
            //m_PXDComputeRaisedBitsImageFilter->SetInput(m_PXDQuicklookGenerator->GetOutput());
            //m_PXDComputeRaisedBitsImageFilter is a filter which computes for each pixel its number of bytes at 1.
            //So the minimum number of bytes at 1 is 0, and the maximum number of bytes at 1 is 8*sizeof(PXDInputImagePixelType)
            vnsLogDebugMacro("ScalarToRainbowVectorImageFilter: Min = 0, Max = "<< (int)(8 * sizeof(PXDInputImagePixelType)))
            m_ScalarToRainbowVectorImageFilter->SetInput(m_PXDQuicklookGenerator->GetOutput());
            m_ScalarToRainbowVectorImageFilter->SetMinimumMaximum(0, (8 * sizeof(PXDInputImagePixelType)));

            // Warning : the caching is used here to free the memory.
            //vnsCachingMacro(m_ScalarToRainbowVectorImageFilterReader, OutputImageType, m_ScalarToRainbowVectorImageFilter->GetOutput(),
                    "QuicklookL3_ScalarToRainbowVectorImageFilter")
            // Then, to avoid having too small streams with the WriteTxtOnImageFilter
            // which would generate errors, the filter is updated
            // to set the complete data in memory
            m_ScalarToRainbowVectorImageFilter->Update();

            if (m_FontSize == 0)
            {
                m_PXDOutputImage = m_ScalarToRainbowVectorImageFilter->GetOutput();
            }
            else
            {
                m_PXDWriteTxtOnImageFilter->SetInput(m_ScalarToRainbowVectorImageFilter->GetOutput());
                m_PXDWriteTxtOnImageFilter->SetText(m_Date + "\nPXD");
                m_PXDWriteTxtOnImageFilter->SetFontSize(m_FontSize);
                m_PXDWriteTxtOnImageFilter->SetFontFileName(m_FontFileName);
                m_PXDWriteTxtOnImageFilter->UpdateData();
                m_PXDOutputImage = m_PXDWriteTxtOnImageFilter->GetOutput();
            }
            ////SRE output
            m_SREMultiChannelExtractROIFilter->SetInput(sreInputImage);
            m_SREMultiChannelExtractROIFilter->ClearChannels();
            m_SREMultiChannelExtractROIFilter->SetChannel(m_QLRedBand);
            m_SREMultiChannelExtractROIFilter->SetChannel(m_QLGreenBand);
            m_SREMultiChannelExtractROIFilter->SetChannel(m_QLBlueBand);

            m_SREChangeValueVectorImageFilter->SetInput(m_SREMultiChannelExtractROIFilter->GetOutput());
            m_SREChangeValueVectorImageFilter->GetFunctor().SetNumberOfComponentsPerPixel(3);
            m_SREChangeValueVectorImageFilter->GetFunctor().SetChange(noDataPixel, inputMin);            //noDataReplacePixel);

            m_SREQuicklookGenerator->SetInput(m_SREChangeValueVectorImageFilter->GetOutput());
            m_SREQuicklookGenerator->SetSubsampleFactor(m_Ratio);
            m_SREQuicklookGenerator->UpdateOutputInformation();

            m_SREVectorRescaleIntensityImageFilter->SetInput(m_SREQuicklookGenerator->GetOutput());
            m_SREVectorRescaleIntensityImageFilter->SetOutputMaximum(outputMax);
            m_SREVectorRescaleIntensityImageFilter->SetOutputMinimum(outputMin);
            m_SREVectorRescaleIntensityImageFilter->SetInputMaximum(inputMax);
            m_SREVectorRescaleIntensityImageFilter->SetInputMinimum(inputMin);
            m_SREVectorRescaleIntensityImageFilter->AutomaticInputMinMaxComputationOff();

            // Warning : the caching is used here to free the memory.
            vnsCachingMacro(m_SREVectorRescaleIntensityImageFilterReader, OutputImageType,
                    m_SREVectorRescaleIntensityImageFilter->GetOutput(), "QuicklookL3_SREVectorRescaleIntensityImageFilter")
            // Then, to avoid having too small streams with the WriteTxtOnImageFilter
            // which would generate errors, the filter is updated
            // to set the complete data in memory
            m_SREVectorRescaleIntensityImageFilter->Update();
            if (m_FontSize == 0)
            {
                m_SREOutputImage = m_SREVectorRescaleIntensityImageFilter->GetOutput();
            }
            else
            {
                m_SREWriteTxtOnImageFilter->SetInput(m_SREVectorRescaleIntensityImageFilter->GetOutput());

                m_SREWriteTxtOnImageFilter->SetText(m_Date + "\nSRE");
                m_SREWriteTxtOnImageFilter->SetFontSize(m_FontSize);
                m_SREWriteTxtOnImageFilter->SetFontFileName(m_FontFileName);
                m_SREWriteTxtOnImageFilter->UpdateData();
                m_SREOutputImage = m_SREWriteTxtOnImageFilter->GetOutput();

            }
            if (m_UseFRE == true)
            {
                InputReflectanceImageConstPointer freInputImage = this->GetFREInputImage();

                ////We do the same thing for FRE output
                m_FREMultiChannelExtractROIFilter->SetInput(freInputImage);
                m_FREMultiChannelExtractROIFilter->ClearChannels();
                m_FREMultiChannelExtractROIFilter->SetChannel(m_QLRedBand);
                m_FREMultiChannelExtractROIFilter->SetChannel(m_QLGreenBand);
                m_FREMultiChannelExtractROIFilter->SetChannel(m_QLBlueBand);

                m_FREChangeValueVectorImageFilter->SetInput(m_FREMultiChannelExtractROIFilter->GetOutput());
                m_FREChangeValueVectorImageFilter->GetFunctor().SetNumberOfComponentsPerPixel(3);
                m_FREChangeValueVectorImageFilter->GetFunctor().SetChange(noDataPixel, inputMin);

                m_FREQuicklookGenerator->SetInput(m_FREChangeValueVectorImageFilter->GetOutput());
                m_FREQuicklookGenerator->SetSubsampleFactor(m_Ratio);

                m_FREVectorRescaleIntensityImageFilter->SetInput(m_FREQuicklookGenerator->GetOutput());
                m_FREVectorRescaleIntensityImageFilter->SetOutputMaximum(outputMax);
                m_FREVectorRescaleIntensityImageFilter->SetOutputMinimum(outputMin);
                m_FREVectorRescaleIntensityImageFilter->SetInputMaximum(inputMax);
                m_FREVectorRescaleIntensityImageFilter->SetInputMinimum(inputMin);
                m_FREVectorRescaleIntensityImageFilter->AutomaticInputMinMaxComputationOff();
                // Warning : the caching is used here to free the memory.
                //vnsCachingMacro(m_FREVectorRescaleIntensityImageFilterReader, OutputImageType,
                        m_FREVectorRescaleIntensityImageFilter->GetOutput(), "QuicklookL3_FREVectorRescaleIntensityImageFilter")
                // Then, to avoid having too small streams with the WriteTxtOnImageFilter
                // which would generate errors, the filter is updated
                // to set the complete data in memory
                m_FREVectorRescaleIntensityImageFilter->Update();
                if (m_FontSize == 0)
                {
                    m_FREOutputImage = m_FREVectorRescaleIntensityImageFilter->GetOutput();
                }
                else
                {
                    m_FREWriteTxtOnImageFilter->SetInput(m_FREVectorRescaleIntensityImageFilter->GetOutput());
                    m_FREWriteTxtOnImageFilter->SetText(m_Date + "\nFRE");
                    m_FREWriteTxtOnImageFilter->SetFontSize(m_FontSize);
                    m_FREWriteTxtOnImageFilter->SetFontFileName(m_FontFileName);
                    m_FREWriteTxtOnImageFilter->UpdateData();
                    m_FREOutputImage = m_FREWriteTxtOnImageFilter->GetOutput();
                }
            }
            else
            {
                vnsLogWarningMacro("Use of FRE image disabled.")
            }
        }

    /** PrintSelf method */
    template<class TInputReflectanceImage, class TPXDTImage, class TOutputImage>
        void
        L3QuicklookTool<TInputReflectanceImage, TPXDTImage, TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL3QuicklookTool_txx */


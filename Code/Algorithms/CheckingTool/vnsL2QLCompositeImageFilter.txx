// clang-format off
/************************************************************************************************************ 
 *                                                                                                          *
 *                                ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo         *
 *                             o                                                                            *
 *                          o                                                                               *
 *                        o                                                                                 *
 *                      o                                                                                   *
 *                     o       ooooooo       ooooooo    o         o      oo                                 *
 *    o              o       o        o     o       o   o         o     o   o                               *
 *      o           o       o          o   o         o  o         o    o      o                             *
 *        o        o       o           o   o         o  o         o    o        o                           *
 *         o      o        o      oooo     o         o  o         o   o           o                         *
 *          o    o          o              o         o  o         o   o           o                         *
 *           o  o            o             o         o  o o      o   o          o                           *
 *            oo              oooooooo    o         o   o  oooooo   o      oooo                             *
 *                                                     o                                                    *
 *                                                     o                                                    *
 *                                                    o                            o                        *
 *                                                    o            o      oooo     o   o      oooo          *
 *                                                   o             o         o    o    o         o          *
 *                                                   o            o       ooo     o   o       ooo           *
 *                                                               o       o       o   o          o           *
 *                                                               ooooo   oooo    o   ooooo  oooo            *
 *                                                                              o                           *
 *                                                                                                          *
 ************************************************************************************************************
// clang-format on
 *                                                                                                          *
 * Author: CS Systemes d'Information  (France)                                                              * 
 *                                                                                                          * 
 ************************************************************************************************************ 
 * HISTORIQUE                                                                                               *
 *                                                                                                          *
 * VERSION : 2.1.0 : DM : LAIG-DM-MAJA-157814-CS : 18 mai 2017 : Refactoring, menage pour ameliorer qualite *
 * VERSION : 2.0.0 : DM : LAIG-DM-MAJA-153176-CS : 3 fevrier 2017 : Modifications pour désactiver           *
 *                          l'incrustation de texte dans les images jpg des produits QCK. Suppression de la *
 *                          gestion de masque dasn les baseline pour que les comparaisons des résultats sur *
 *                          les redhat 5, 6 et 7 soient plus simples à géréer                               *
 * VERSION : 1.0.0 : FA : LAIG-FA-MAC-148399-CS : 03 novembre 2016 : Audit code - Correction qualite        *
 * VERSION : 4-7-3 : FA : LAIG-FA-MAC-141974-CS : 15 fevrier 2016 : Controle sur les bornes Min et Max pour *
 *                                                                 l' adaptation de la dynamique de l'image *
 * VERSION : 4-0-0 : FA : LAIG-FA-MAC-120653-CS : 7 mai 2014 : Correction de règles qualité ou commentaires *
 * VERSION : 1-0-0 : <TypeFT> : <NumFT> : 24 sept. 2010 : Creation                                                           
 *                                                                                                          *
 * FIN-HISTORIQUE                                                                                           *
 *                                                                                                          *
 * $Id$
 *                                                                                                          *
 ************************************************************************************************************/
#ifndef __vnsL2QLCompositeImageFilter_txx
#define __vnsL2QLCompositeImageFilter_txx

#include "vnsL2QLCompositeImageFilter.h"
//#include "vnsCaching.h"
#include "vnsMacro.h"

namespace vns
{

    /** Constructor */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        L2QLCompositeImageFilter<TInputImage, TMaskImage, TOutputImage>::L2QLCompositeImageFilter() :
                m_BackgroundMaskValue(0), m_Ratio(1), m_MinValue(static_cast<InputImageInternalPixelType>(0)), m_MaxValue(
                        static_cast<InputImageInternalPixelType>(0)), m_NoData(static_cast<InputImageInternalPixelType>(-1000)), m_NoDataReplaceValue(
                        static_cast<InputImageInternalPixelType>(0))

        {
            /** set number of required inputs */
            this->SetNumberOfRequiredInputs(4);
            this->SetNumberOfIndexedInputs(5);

            // Shadow label
            m_ShadowLabel.SetSize(3);
            m_ShadowLabel.Fill(0);
            m_ShadowLabel[1] = 255;
            m_CloudLabel.SetSize(3);
            // Cloud label
            m_CloudLabel.Fill(0);
            m_CloudLabel[0] = 255;
            // Water Label
            m_WaterLabel.SetSize(3);
            m_WaterLabel.Fill(255);
            m_WaterLabel[0] = 0;
            // Snow Label
            m_SnowLabel.SetSize(3);
            m_SnowLabel.Fill(255);

            /** Create filters */
            m_ScalarToRainbowFilter = ScalarToRainbowVectorImageFilterType::New();
            m_PrintCLDFilter = PrintableImageFilterType::New();
            m_PrintWATFilter = PrintableImageFilterType::New();
            m_PrintSHDFilter = PrintableImageFilterType::New();
            m_QuicklookGenerator = QuicklookImageGeneratorType::New();

            m_CloudQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_WaterQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_ShadowQuicklookGenerator = QuicklookMaskGeneratorType::New();
            m_SnowQuicklookGenerator = QuicklookMaskGeneratorType::New();

            m_CLDExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_WATExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_SHDExtractEdgeFilter = L2QLExtractEdgeFilterType::New();
            m_SNWExtractEdgeFilter = L2QLExtractEdgeFilterType::New();

            m_PrintSNWFilter = PrintableImageFilterType::New();

            // m_PrintMaskReader = PrintSHDReaderType::New();

            m_WriteTxtOnImageFilter = WriteTxtOnImageFilterType::New();

            m_OutputImage = OutputImageType::New();

        }

    /** Destructor */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        L2QLCompositeImageFilter<TInputImage, TMaskImage, TOutputImage>::~L2QLCompositeImageFilter()
        {
        }

    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeImageFilter<TInputImage, TMaskImage, TOutputImage>::GenerateOutputInformation(void)
        {
            Superclass::GenerateOutputInformation();

            // Grab the input
            InputImageConstPointer inputPtr = this->GetInputImage();
            OutputImagePointer outputPtr = this->GetOutput();

            // Check the validty of the output pointer
            if (!outputPtr)
            {
                vnsExceptionBusinessMacro("Internal error: Invalid output pointer.");
            }

            // Check ratio validity
            if (m_Ratio == 0)
            {
                vnsExceptionBusinessMacro("Invalid ratio. Ratio can't be null.");
            }

            // Set the parameters to the CloudQuicklloGenerator filter
            m_CloudQuicklookGenerator->SetInput(this->GetInputCLDMask());
            m_CloudQuicklookGenerator->SetSubsampleFactor(m_Ratio);
            m_CloudQuicklookGenerator->UpdateOutputInformation();

            // Set the region and components to the output
            outputPtr->SetLargestPossibleRegion(m_CloudQuicklookGenerator->GetOutput()->GetLargestPossibleRegion());
            outputPtr->SetNumberOfComponentsPerPixel(3);
        }

    /** Generate Data */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeImageFilter<TInputImage, TMaskImage, TOutputImage>::UpdateData()
        {
            // Grab the input
            InputImageConstPointer inputImage = this->GetInputImage();
            MaskImageConstPointer inputWATMask = this->GetInputWATMask();
            MaskImageConstPointer inputCLDMask = this->GetInputCLDMask();
            MaskImageConstPointer inputSHDMask = this->GetInputSHDMask();
            MaskImageConstPointer inputSNWMask = this->GetInputSNWMask();

            // Generate cloud mask quick look
            m_CloudQuicklookGenerator->SetInput(inputCLDMask);
            m_CloudQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            // Generate water mask quick look
            m_WaterQuicklookGenerator->SetInput(inputWATMask);
            m_WaterQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            // Generate shadow mask quick look
            m_ShadowQuicklookGenerator->SetInput(inputSHDMask);
            m_ShadowQuicklookGenerator->SetSubsampleFactor(m_Ratio);

            // Generate edge mask
            m_CLDExtractEdgeFilter->SetInput(m_CloudQuicklookGenerator->GetOutput());
            m_WATExtractEdgeFilter->SetInput(m_WaterQuicklookGenerator->GetOutput());
            m_SHDExtractEdgeFilter->SetInput(m_ShadowQuicklookGenerator->GetOutput());

            m_CLDExtractEdgeFilter->UpdateData();
            m_WATExtractEdgeFilter->UpdateData();
            m_SHDExtractEdgeFilter->UpdateData();

            // Generate input quick look
            m_QuicklookGenerator->SetInput(inputImage);
            m_QuicklookGenerator->SetSubsampleFactor(m_Ratio);
            m_QuicklookGenerator->GenerateOutputInformation();

            // Rainbow effect
            m_ScalarToRainbowFilter->SetInput(m_QuicklookGenerator->GetOutput());
            m_ScalarToRainbowFilter->SetMinimumMaximum(m_MinValue, m_MaxValue);

            // Add cloud
            
            m_PrintCLDFilter->SetInput1(m_ScalarToRainbowFilter->GetOutput());
            m_PrintCLDFilter->SetInput2(m_CLDExtractEdgeFilter->GetOutput());
            m_PrintCLDFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintCLDFilter->GetFunctor().SetChange(m_CloudLabel);
   
            // Add water
            m_PrintWATFilter->SetInput1(m_PrintCLDFilter->GetOutput());
            m_PrintWATFilter->SetInput2(m_WATExtractEdgeFilter->GetOutput());
            m_PrintWATFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintWATFilter->GetFunctor().SetChange(m_WaterLabel);
 
            // Add shadows
            m_PrintSHDFilter->SetInput1(m_PrintWATFilter->GetOutput());
            m_PrintSHDFilter->SetInput2(m_SHDExtractEdgeFilter->GetOutput());
            m_PrintSHDFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
            m_PrintSHDFilter->GetFunctor().SetChange(m_ShadowLabel);

            

            if (inputSNWMask.IsNull() == false)
            {
    
                m_SnowQuicklookGenerator->SetInput(inputSNWMask);
                m_SnowQuicklookGenerator->SetSubsampleFactor(m_Ratio);

                m_SNWExtractEdgeFilter->SetInput(m_SnowQuicklookGenerator->GetOutput());
                m_SNWExtractEdgeFilter->UpdateData();

                m_PrintSNWFilter->SetInput1(m_PrintSHDFilter->GetOutput());
                m_PrintSNWFilter->SetInput2(m_SNWExtractEdgeFilter->GetOutput());
                m_PrintSNWFilter->GetFunctor().SetBackgroundMaskValue(m_BackgroundMaskValue);
                m_PrintSNWFilter->GetFunctor().SetChange(m_SnowLabel);

                // Then, to avoid having too small streams with the WriteTxtOnImageFilter
                // which would generate errors, the filter PrintSHDFilter is updated
                // to set the complete data in memory
                m_PrintSNWFilter->Update();

                // Write or not the text in the output image
                if (m_WriteTxtOnImageFilter->GetFontSize() == 0)
                {
                    m_OutputImage = m_PrintSNWFilter->GetOutput();
                }
                else
                {
                    // Add the text in the upper right image corner
                    m_WriteTxtOnImageFilter->SetInput(m_PrintSNWFilter->GetOutput());
                    m_WriteTxtOnImageFilter->UpdateData();
                    m_OutputImage = m_WriteTxtOnImageFilter->GetOutput();
                }

            }
            else
            {
                // Then, to avoid having too small streams with the WriteTxtOnImageFilter
                // which would generate errors, the filter PrintSHDFilter is updated
                // to set the complete data in memory
                m_PrintSHDFilter->Update();

                // Write or not the text in the output image
                if (m_WriteTxtOnImageFilter->GetFontSize() == 0)
                {
                    m_OutputImage = m_PrintSHDFilter->GetOutput();
                }
                else
                {
                    // Add the text in the upper right image corner
                    m_WriteTxtOnImageFilter->SetInput(m_PrintSHDFilter->GetOutput());
                    m_WriteTxtOnImageFilter->UpdateData();
                    m_OutputImage = m_WriteTxtOnImageFilter->GetOutput();
                }
                

            }

        }

    /** PrintSelf method */
    template<class TInputImage, class TMaskImage, class TOutputImage>
        void
        L2QLCompositeImageFilter<TInputImage, TMaskImage, TOutputImage>::PrintSelf(std::ostream& os, itk::Indent indent) const
        {
            this->Superclass::PrintSelf(os, indent);
        }

} // End namespace vns

#endif /* __vnsL2QLCompositeImageFilter_txx */

apTvWriteTxtOnImage
~~~~~~~~~~~~~~~~~~~~~~~~

Objectif
********
Validation de l'application "WriteTxtOnImage"

Description
***********

Le module "WriteTxtOnImage" permet d'écrire du texte sur une image GeoTiff.


Liste des données d’entrées
***************************
.. code-block::
    
    -in L930662_20020212_L7_198_030_USGS_l2cld_240m.tif
    -font Amble-Italic.ttf
    -fontsize 20
    -text "20210804"
    -red 255
    -blue 0
    -green 0


Liste des produits de sortie
****************************

-out apTvWriteTxtOnImage.tif uint8


Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0

Vérifications à effectuer
**************************
Le test génère une image avec le texte indiqué en paramètre.

Mise en oeuvre du test
**********************
Ce test est exécuté en lançant la commande :
ctest -R apTvWriteTxtOnImage

Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
04/08/2021             OK
================== =================

Exigences
*********
Ce test couvre les exigences suivantes :
Néant


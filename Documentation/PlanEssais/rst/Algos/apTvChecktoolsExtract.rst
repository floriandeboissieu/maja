apTvChecktoolsExtract
~~~~~~~~~~~~~~~~~~~~~~~~

Objectif
********
Validation de l'application "Extract"

Description
***********

Le module "Extract" permet d'extraire des statistiques d'un produit L2A telles que des moyennes de SRE, AOT, VAP et FRE autour d'un pixel déterminé en entrée. Des indicateurs de validité de ces stats sont fournis en sortie également


Liste des données d’entrées
***************************
.. code-block::
    -aot ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_aot_scale_R2.tif
    -sre ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_sre_scale_R2.tif
    -fre ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_fre_scale_R2.tif
    -vap ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_vap_scale_R2.tif
    -cld ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_Cloud_Mask_all_R2.tif
    -wat ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_wat_R2.tif
    -edg ${MAJADATA_TVA_INPUT}/CheckingTools/SENTINEL2B_20171010-105012-463_L2A_T31TCH_C_V1-0_EDG_R2.tif
    -refsre ${MAJADATA_TVA_INPUT}/CheckingTools/tmp_sre_scale_R1.tif
    -listbandcode B5 B6 B7 B8A B11 B12
    -bandidsl2coarse 4 5 6 8 11 12
    -nbbands 13
    -currentpixel.x 2.368117
    -currentpixel.y 51.03535
    -currentpixel.freename "Dunkerque"
    -currentpixel.unit "degree"
    -radius 2
    -aotradius 1000
    -vapradius 100
    -usefre true
    -nodata -100000000.0
    -aotnodata 0.0
    -vapnodata 0.0


Liste des produits de sortie
****************************

-out apTvExtractFlag.txt


Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0

Vérifications à effectuer
**************************
Le test génère en sortie une liste de valeurs contenant les moyennes de SRE / FRE / AOT / VAP et les flags de validité de ces stats.
Il permet également de savoir le pourcentage de pixels valides / utiles.

Mise en oeuvre du test
**********************
Ce test est exécuté en lançant la commande :
ctest -R apTvChecktoolsExtract

Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
04/08/2021             OK
================== =================

Exigences
*********
Ce test couvre les exigences suivantes :
Néant


pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD_CHAIN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Titre
*****
Validation fonctionnelle et numérique de la chaîne L2 (Pre-PostProcessing, entrée/sorties) avec un produit SENTINEL2 natif PDGS nouveau format 14.8 en mode INIT. Les paramètres de la couche Standalone sont passés en ligne de commande.

Objectif
********

L’objectif de cet essai est de valider le fonctionnement global de la chaîne L2 en mode INIT sur des produits SENTINEL2 natifs PDGS au format 14.8 comportant différentes résolutions. 
Le produit L1 fourni en entrée sont conformes au document S2-PDGS-TAS-DI-PSD-V14.8 (Nouveau format). Le format de sortie est earth explorer.



Description
***********

Les options de traitement sont :

- Méthode SPECTROTEMPORAL pour l’estimation des aérosols,
- Correction de l’environnement et des pentes,
- Ecriture à la résolution L2.



Liste des données d’entrées
***************************

Produits :

- S2B_MSIL1C_20210517T103619_N7990_R008_T30QVE_20210929T075738.SAFE                   
- S2__TEST_AUX_REFDE2_30QVE_1001.HDR
- S2__TEST_AUX_REFDE2_30QVE_1001.DBL.DIR

Gipps:

- S2B_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.DBL.DIR
- S2B_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.DBL.DIR
- S2B_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.DBL.DIR
- S2B_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.DBL.DIR
- S2B_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.DBL.DIR
- S2__TEST_GIP_L2SITE_S_ALLSITES_00001_20190626_21000101.EEF
- S2B_TEST_GIP_CKEXTL_S_ALLSITES_00001_20190626_21000101.EEF
- S2B_TEST_GIP_CKQLTL_S_ALLSITES_00001_20190626_21000101.EEF
- S2B_TEST_GIP_L2COMM_L_ALLSITES_00001_20190626_21000101.EEF
- S2B_TEST_GIP_L2ALBD_L_CONTINEN_00001_20190626_21000101.HDR
- S2B_TEST_GIP_L2DIFT_L_CONTINEN_00001_20190626_21000101.HDR
- S2B_TEST_GIP_L2DIRT_L_CONTINEN_00001_20190626_21000101.HDR
- S2B_TEST_GIP_L2TOCR_L_CONTINEN_00001_20190626_21000101.HDR
- S2B_TEST_GIP_L2WATV_L_CONTINEN_00001_20190626_21000101.HDR
- S2B_TEST_GIP_L2SMAC_L_ALLSITES_00001_20190626_21000101.EEF


Liste des produits de sortie
****************************

Produit L2A SENTINEL2 Natif

Prérequis
*********
Il n’y a pas de prérequis.

Durée attendue
***************
La durée d’exécution de l’essai n’est pas un critère attendu.

Epsilon utilisé sur la non regression
*************************************
0.0001

Vérifications à effectuer
**************************

Les tests COMP_ASCII et COMP_IMAGE associés permettent de valider la non regression.

Mise en oeuvre du test
**********************

Ce test est exécuté en lançant la commande :
ctest -R pyTv-S2-L2INIT-001-SENTINEL2-NEWPSD_CHAIN

Exigences
*********
Ce test couvre les exigences suivantes :
MACCS-Exigence 50 (C) ; MACCS-Exigence 550 (C) ; MACCS-Exigence 630 (C) ;



Journal d’essai de la recette
*****************************

Notes sur le déroulement du test
--------------------------------
Rien de particulier n’a été noté lors du déroulement du test.

Conclusion du déroulement du test
---------------------------------
RAS

Validation du test
------------------

================== =================
Date de validation    Résultat
25/10/2021              OK
================== =================


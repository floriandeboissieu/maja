
.. figure:: Art/image1.png

**CS Systèmes d'Information**

| Business Unit ESPACE                                                                                          
| Département Payload Data & Applications    
| Image Processing		   


.. tabularcolumns:: |L|J|J|J|p{0.6in}|
		    
=============================================== == ================= ============
Software: **MAJA**
**SETG-**\ **PE-**\ **MAJA-**\ **020-**\ **CS**                      
Change                                          02 Date              29/11/2019
Issue                                           02 Date              05/05/2020
Distribution Code                               E
Ref. : CSSI/SPACE/PD&A/MAJA/DCG 
=============================================== == ================= ============

**Plan d’Intégration et de Validation [PE]**

=================================== ===============
Rédigé par :                        le : 05/05/2020
                                                   
ESQUIS Benjamin  CSSI/ESPACE/PDA/IP                
Validé par :                        le : 05/05/2020
                                                   
OLIVIE Francis CSSI/ESPACE/DSM                     
Pour application :                  le : 05/05/2020
                                                   
ESQUIS Benjamin CSSI/ESPACE/PDA/IP                
=================================== ===============

**Bordereau d'indexation**

+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| Confidentialité :                                                                      | Mots clés : Conception, MAJA, Sentinel2, VENµS, chaînes L2                                   |
| DLP                                                                                    |                                                                                              |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| TITRE Du Document :                                                                                                                                                                   |
| Plan d’Intégration et de Validation                                                                                                                                                   |
| MAJA                                                                                                                                                                                  |
| [PE]                                                                                                                                                                                  |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
|   AUTEUR(s):                                                                           |                                                                                              |
|                                                                                        | | ESQUIS Benjamin  CSSI/ESPACE/PDA/IP                                                        |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| RESUME : Ce document présente l’ensemble des tests et moyens mis en œuvre par l’équipe de développement pour mener à bien les différents essais à mener au cours de la réalisation du |
| projet MAJA                                                                                                                                                                           |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| Documents Rattaches : Le document vit seul                                             | LOCALISATION :                                                                               |
|                                                                                        | CSSI/SPACE/PD&A/MAJA                                                                         |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| Volume : 1                                                                             | Nombre total de pages : N/A                         | Doc composite : N      | LANGUAGE : FR |
|                                                                                        |                                                     |                        |               |
|                                                                                        | Dont pages préliminaires : 0                        |                        |               |
|                                                                                        |                                                     |                        |               |
|                                                                                        | Nombre de pages suppl : 0                           |                        |               |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| GESTION DE CONF. : NG                                                                  | CM RESP. :                                                                                   |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| CAUSE D'EVOLUTION : Création pour MAJA v4.4.0                                                                                                                                         |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+
| CONTRAT : Marché ACSIS n°181112                                                                                                                                                       |
+----------------------------------------------------------------------------------------+-----------------------------------------------------+------------------------+---------------+

**Diffusion interne**

================== ====================
ESQUIS Benjamin    CSSI/ESPACE/PDA/IP   
BROSSARD Julie     CSSI/ESPACE/PDA/IP
ROMAIN Thibaut     CSSI/ESPACE/PDA/IP   
OLIVIE Francis     CSSI/ESPACE/DSM      
RECULEAU SERGE     CSSI/ESPACE/PDA/PDGS 
\                                       
\                                       
\                                       
\                                       
\                                       
\                                       
\                                       
\                                       
\                                       
================== ====================

**Diffusion externe**

=================== ======================== ============
Name                Entity                   Observations
SYLVANDER Sylvia    DTN/CD/ID                
HAGOLLE Olivier     DTN/CD/CB                
KETTIG Peter        DTN/CD/SA
AMIOT Carole        DTN/CD/ID             
LARIF Marie-France  DTN/CD/TPA
SPECHT Bernard      DTN/CD/TPA
GUIBERT Sarah       DTN/CD/SA       
VANICAT Zara        DTN/QE/NEO               
\                                            
=================== ======================== ============


**Modification**

.. tabularcolumns:: |p{1cm}|L|p{2cm}|L|

=== ==== ========== ================================================================================
Ed. Rév. Date       Référence, Auteur(s), Causes d’évolution
04  04   06/08/2021 CSSI/ESPACE/PDA/IP/MAJA/PE
                   
                    Equipe MAJA
                   
                    Mise a jour MAJA 4.4
                   
                    Ce document passe directement de la version 4.0 à la version 4.4 pour être en phase avec
                   
                    le numéro de la version logicielle.
				   
01  00   30/04/2020 CSSI/ESPACE/PDA/IP/MAJA/PE
                   
                    ESQUIS Benjamin CSSI/ESPACE/PDA/IP
                   
                    Création pour MAJA 4.2

=== ==== ========== ================================================================================

Sommaire
========

.. toctree::
	:maxdepth: 5
	:caption: Glossaire et Liste des paramètres AC \& AD 1
	
	Glossaire

.. toctree::
	:maxdepth: 5
	:caption: Généralités
	
	Généralités


.. toctree::
	:maxdepth: 5
	:caption: Introduction
	
	Introduction

.. toctree::
	:maxdepth: 5
	:caption: Environnement de validation
	
	Environnement

.. toctree::
	:maxdepth: 5
	:caption: Tests unitaires
	
	TestsUnitaires

.. toctree::
	:maxdepth: 5
	:caption: Tests des algorithmes
	
	Algorithms

.. toctree::
	:maxdepth: 5
	:caption: Tests de validation
	
	Validation

.. toctree::
	:maxdepth: 5
	:caption: Annexes
	
	Annexes

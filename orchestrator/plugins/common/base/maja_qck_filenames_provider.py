# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Centre National d'Etudes Spatiales (CNES)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
###################################################################################################

                        o     o
                        oo   oo   oo        o   oo        ,-.
                        o o o o  o  o       o  o  o       \_/
                        o  o  o o    o      o o    o     {|||D
                        o     o oooooo      o oooooo      / \
                        o     o o    o o    o o    o      `-^
                        o     o o    o  oooo  o    o

###################################################################################################
orchestrator.plugins.common.base.maja_l_2_3_image_filenames_provider_base -- shortdesc

orchestrator.plugins.common.base.maja_l_2_3_image_filenames_provider_base is a description

It defines


###################################################################################################
"""

import os
from orchestrator.common.maja_exceptions import (
    MajaDataException,
    MajaPluginBaseException,
)
from orchestrator.common.earth_explorer.earth_explorer_utilities import (
    EarthExplorerUtilities,
)
from orchestrator.common.earth_explorer.header_image_earth_explorer_xml_file_handler import (
    HeaderImageEarthExplorerXMLFileHandler,
)
from orchestrator.common.date_utils import get_datetime_from_yyyymmdd
from orchestrator.common.logger.maja_logging import configure_logger
from orchestrator.common.gipp_utils import uncompress_dbl_product
import orchestrator.common.maja_common as maja_common
import orchestrator.common.xml_tools as xml_tools
import orchestrator.common.file_utils as file_utils
from orchestrator.common.date_utils import get_julianday_as_double

LOGGER = configure_logger(__name__)


class QCKFilenamesProvider(object):
    """ """

    def __init__(self):
        self.m_HDRFileName = ""
        self.m_DBLDirectory = ""
        self.m_File_Type = ""
        self.m_File_Class = ""
        self.HDRRefName = ""
        self.m_list_extract_files = []
        self.m_list_quicklook_files = []

    # Set the reference filenames HDR and DBL.DIR
    def initialize(self, p_InputL2ImageFileReader, outputdir):
        m_LevelType = p_InputL2ImageFileReader.LevelType
        self.m_File_Type = "QCK_" + m_LevelType
        self.m_File_Class = p_InputL2ImageFileReader.FileClass
        m_Site = p_InputL2ImageFileReader.Nick_Name
        m_Date = p_InputL2ImageFileReader.Acquisition_Date
        LOGGER.debug(m_Date)

        self.HDRRefName = (
            p_InputL2ImageFileReader.Prefix
            + "_"
            + self.m_File_Class
            + "_QCK_"
            + m_LevelType
            + "_"
            + m_Site
            + "_"
            + m_Date
        )
        self.m_HDRFileName = os.path.join(outputdir, self.HDRRefName + ".HDR")
        LOGGER.debug(self.m_HDRFileName)
        self.m_DBLDirectory = os.path.join(outputdir, self.HDRRefName + ".DBL.DIR")
        LOGGER.debug("Creating directory %s", self.m_DBLDirectory)
        file_utils.create_directory(self.m_DBLDirectory)

    def get_file_class(self):
        return self.m_File_Class

    def get_file_type(self):
        return self.m_File_Type

    def get_header_filename_fullpath(self):
        return self.m_HDRFileName

    def get_header_filename(self):
        return self.HDRRefName

    def get_dbl_dir(self):
        return self.m_DBLDirectory

    def add_extractpoint_filename(self, extract_filename):
        l_ext_file = EarthExplorerUtilities.get_regular_file_from_file(extract_filename)
        l_ext_file.LogicalName = os.path.basename(l_ext_file.LogicalName)
        self.m_list_extract_files.append(l_ext_file)

    def add_quicklook_filename(self, qkl_filename):
        l_qlk_file = EarthExplorerUtilities.get_regular_file_from_file(qkl_filename)
        l_qlk_file.LogicalName = os.path.basename(l_qlk_file.LogicalName)
        self.m_list_quicklook_files.append(l_qlk_file)

    def get_list_of_infos_files_extract(self):
        return self.m_list_extract_files

    def get_list_of_infos_files_quicklook(self):
        return self.m_list_quicklook_files
